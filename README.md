# NDD Test4J

NDD Test4J provides testing support. Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

Since this library is a collection of mostly unrelated testing tools, please check the [Test4J Javadoc](https://ndd-java.bitbucket.org/ndd-test4j/apidocs/index.html). Provided packages are:

- `name.didier.david.test4j.assertions`
- `name.didier.david.test4j.coverage`
- `name.didier.david.test4j.testng` is certainly the most useful
- `name.didier.david.test4j.utils`

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-test4j</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-test4j/project-reports.html](https://ndd-java.bitbucket.org/ndd-test4j/project-reports.html)

## About

Copyright David DIDIER 2014
