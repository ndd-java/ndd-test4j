# NDD Test4J

## Version 0.4.3

- [FEATURE] Add HTTP codes to ResponseAssert

## Version 0.4.2

- [BUG] Add better reporting for DbUnitTestSupport test methods
- [FEATURE] Add ResponseAssert#hasJsonBody
- [FEATURE] Add JsonUtils
- [FEATURE] Add assertions for JAR-RS data types

## Version 0.4.1

- [FEATURE] Add DbUnitTestSupport
- [FEATURE] Add DatabaseSchemaExporter for DbUnit
- [FEATURE] Add toPath to the TestResource framework
- [QUALITY] Fix CheckStyle warnings and improve code coverage

## Version 0.4.0

- [FEATURE] Replace TestNgMockitoListener with semi official version
- [FEATURE] Add a JUnit implementation of @TestResource
- [FEATURE] Add JUnitDataProviders
- [DEPENDENCY] Update parent POM to use Java 8
- [DEPENDENCY] Fix errors following dependencies update
- [QUALITY] Fix CheckStyle error
- [QUALITY] Refactor @TestResource framework

## Version 0.3.6

- [DEPENDENCY] Updated the version of the `ndd-parent-java` dependency to `0.2.4`
- [QUALITY] Updated Checkstyle configuration to version 6.3

## Version 0.3.5

- [QUALITY] Added PMD report, see [NDD-TEST4J-5](https://bitbucket.org/ndd-java/ndd-test4j/issue/5/add-pmd-report)
- [FEATURE] Added data providers for empty collections, see [NDD-TEST4J-4](https://bitbucket.org/ndd-java/ndd-test4j/issue/4/add-data-providers-for-empty-collections)
- [QUALITY] Fixed issues reported by FindBugs

## Version 0.3.4

- [FEATURE] Support injection of `InputStream` and `InputStreamReader` by `TestNgResourceListener`, see [NDD-TEST4J-3](https://bitbucket.org/ndd-java/ndd-test4j/issue/3)
- [FEATURE] Added `ThreadTestContext`
- [QUALITY] Replaced Guava `Charsets` with NIO `StandardCharsets`
- [QUALITY] Removed `JUnit` dependency

## Version 0.3.3

- [BUG] Renamed `ExitCaptorChecker` to `ExitCaptureJob`
- [BUG] Renamed `StdCaptorChecker` to `StdCaptureJob`
- [FEATURE] Added `CliCaptureJob`
- [FEATURE] Added content injection to `@TestResource`
- [QUALITY] Refactored `@TestResource` _mini framework_

## Version 0.3.2

- [BUG] Replaced return type of `TestNgDataProviders#hmacXXX` from Object to String
- [FEATURE] Added `TestNgDataProviders#emptyButNotNullStrings`
- [FEATURE] Added `StdCaptor` and `StdCaptorChecker`
- [FEATURE] Added `ExitCaptor` and `ExitCaptorChecker`

## Version 0.3.1

- [FEATURE] Added HMAC providers in `TestNgDataProviders`

## Version 0.3.0

- [BUILD] Created the new project `ndd-parent-java`
- [FEATURE] Added `TestNgDataProviders` for strings, numbers and files
- [FEATURE] Added `TestResource` injection facility
- [QUALITY] Added Checkstyle import control check

## Version 0.2.0

- [BUG] Fixed POM versioning

## Version 0.1.0

- Initial commit
