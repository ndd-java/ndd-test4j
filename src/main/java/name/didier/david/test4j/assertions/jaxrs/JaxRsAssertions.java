package name.didier.david.test4j.assertions.jaxrs;

import javax.ws.rs.core.Response;

/**
 * Entry point for assertion methods for JAX-RS data types.
 *
 * @author ddidier
 */
public class JaxRsAssertions {

    /** Default constructor. */
    protected JaxRsAssertions() {
        super();
    }

    /**
     * Creates a new instance of <code>{@link ResponseAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    public static ResponseAssert assertThat(Response actual) {
        return ResponseAssert.assertThat(actual);
    }
}
