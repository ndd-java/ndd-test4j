package name.didier.david.test4j.assertions;

/**
 * Assertions enforcing coding standard, using AssertJ.
 * 
 * @author ddidier
 */
public class CodingStandardAssertions {

    /**
     * Utility class.
     */
    protected CodingStandardAssertions() {
        super();
    }

    /**
     * Creates a new instance of <code>{@link CodingStandardClassAssert}</code>.
     * 
     * @param actual the actual value.
     * @return the created assertion object.
     */
    public static CodingStandardClassAssert assertThat(final Class<?> actual) {
        return new CodingStandardClassAssert(actual);
    }
}
