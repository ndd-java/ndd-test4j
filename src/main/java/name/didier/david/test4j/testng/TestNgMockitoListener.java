package name.didier.david.test4j.testng;

import static org.mockito.internal.util.reflection.Fields.annotatedBy;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.internal.configuration.CaptorAnnotationProcessor;
import org.mockito.internal.util.reflection.Fields;
import org.mockito.internal.util.reflection.InstanceField;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * <b>WARNING: @Spy isn't working with HashMap!?!?</b>
 *
 * <p>
 * Mockito TestNG Listener, this listener adds the following behavior to your test :
 * </p>
 * <ul>
 * <li>Initializes mocks annotated with {@link org.mockito.Mock}, so that <strong>explicit usage of
 * {@link org.mockito.MockitoAnnotations#initMocks(Object)} is not necessary</strong>. <strong>Note :</strong> With
 * TestNG, mocks are initialized before any TestNG method, either a <em>configuration method</em> (&#064;BeforeMethod,
 * &#064;BeforeClass, etc) or a <em>test</em> method, i.e. mocks are initialized once only once for each test instance.
 * </li>
 * <li>As mocks are initialized only once, they will be reset after each <em>test method</em>. See Javadoc
 * {@link org.mockito.Mockito#reset(Object[])}</li>
 * <li>Validates framework usage after each test method. See Javadoc for
 * {@link org.mockito.Mockito#validateMockitoUsage()}.</li>
 * </ul>
 *
 * <p>
 * The listener is completely optional - there are other ways you can get &#064;Mock working, for example by writing a
 * base class. Explicitly validating framework usage is also optional because it is triggered automatically by Mockito
 * every time you use the framework. See Javadoc for {@link org.mockito.Mockito#validateMockitoUsage()}.
 * </p>
 *
 * <p>
 * Read more about &#064;Mock annotation in Javadoc for {@link org.mockito.MockitoAnnotations}.
 * </p>
 *
 * <pre class="code">
 * <code class="java">
 * <b>&#064;Listeners(TestNgMockitoListener.class)</b>
 * public class ExampleTest {
 *
 *     &#064;Mock
 *     private List list;
 *
 *     &#064;Test
 *     public void shouldDoSomething() {
 *         list.add(100);
 *     }
 * }
 * </code>
 * </pre>
 *
 * @author https://github.com/mockito/mockito/tree/master/subprojects/testng
 * @author ddidier
 */
public class TestNgMockitoListener
        implements IInvokedMethodListener {

    /** Actions to execute before the test. */
    private final BeforeTestMethod beforeTest = new BeforeTestMethod();
    /** Actions to execute after the test. */
    private final AfterTestMethod afterTest = new AfterTestMethod();

    /** Default constructor. */
    public TestNgMockitoListener() {
        super();
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (hasTestNgMockitoListenerInTestHierarchy(testResult.getTestClass().getRealClass())) {
            beforeTest.applyFor(method, testResult);
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (hasTestNgMockitoListenerInTestHierarchy(testResult.getTestClass().getRealClass())) {
            afterTest.applyFor(method, testResult);
        }
    }

    /**
     * @param testClass the class being tested for the {@link org.testng.annotations.Listeners} annotation.
     * @return <code>true</code> if the specified test class or one of its superclass is annotated with
     *         {@link org.testng.annotations.Listeners} referencing {@link TestNgMockitoListener}, <code>false</code>
     *         otherwise.
     */
    protected boolean hasTestNgMockitoListenerInTestHierarchy(Class<?> testClass) {
        return TestNgListenerUtils.isListening(testClass, TestNgMockitoListener.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Actions to execute before the test.
     */
    protected static class BeforeTestMethod {

        /** The initialized test instances. */
        private final WeakHashMap<Object, Boolean> initializedInstances = new WeakHashMap<>();

        /** Default constructor. */
        public BeforeTestMethod() {
            super();
        }

        /**
         * Initialize mocks.
         *
         * @param testMethod the invoked test method.
         * @param testResult metadata about the class under test.
         */
        public void applyFor(IInvokedMethod testMethod, ITestResult testResult) {
            initializeMocks(testResult);
            resetCaptors(testMethod, testResult);
        }

        /**
         * Initialize all the mocks of the test instance.
         *
         * @param testResult metadata about the class under test.
         */
        private void initializeMocks(ITestResult testResult) {
            Object testInstance = testResult.getInstance();
            if (!alreadyInitialized(testInstance)) {
                MockitoAnnotations.initMocks(testInstance);
                markAsInitialized(testInstance);
            }
        }

        /**
         * Reset all the arguments captors of the test instance.
         *
         * @param testMethod the invoked test method.
         * @param testResult metadata about the class under test.
         */
        private void resetCaptors(IInvokedMethod testMethod, ITestResult testResult) {
            if (!testMethod.isConfigurationMethod()) {

                @SuppressWarnings("unchecked")
                List<InstanceField> instanceFields = Fields
                        .allDeclaredFieldsOf(testResult.getInstance())
                        .filter(annotatedBy(Captor.class))
                        .instanceFields();

                for (InstanceField instanceField : instanceFields) {
                    Captor captor = instanceField.annotation(Captor.class);
                    Field field = instanceField.jdkField();
                    instanceField.set(new CaptorAnnotationProcessor().process(captor, field));
                }
            }
        }

        /**
         * @param testInstance the test instance to mark as initialized.
         */
        private void markAsInitialized(Object testInstance) {
            initializedInstances.put(testInstance, true);
        }

        /**
         * @param testInstance the test instance to test.
         * @return <code>true</code> if the given instance has already been initialized, <code>false</code> otherwise.
         */
        private boolean alreadyInitialized(Object testInstance) {
            return initializedInstances.containsKey(testInstance);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Actions to execute after the test.
     */
    protected static class AfterTestMethod {

        /** Default constructor. */
        public AfterTestMethod() {
            super();
        }

        /**
         * Validate Mockito usage and reset mocks.
         *
         * @param testMethod the invoked test method.
         * @param testResult metadata about the class under test.
         */
        public void applyFor(IInvokedMethod testMethod, ITestResult testResult) {
            Mockito.validateMockitoUsage();

            if (testMethod.isTestMethod()) {
                Object testInstance = testResult.getInstance();
                Mockito.reset(instanceMocksOf(testInstance).toArray());
            }
        }

        /**
         * @param testInstance the test instance.
         * @return all the values of the fields of the test instance annotated with {@link Mock} or {@link Spy}.
         */
        @SuppressWarnings("unchecked")
        private Collection<Object> instanceMocksOf(Object testInstance) {
            return Fields.allDeclaredFieldsOf(testInstance)
                    .filter(annotatedBy(Mock.class, Spy.class))
                    .notNull()
                    .assignedValues();
        }
    }
}
