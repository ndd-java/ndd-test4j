package name.didier.david.test4j.testng;

/**
 * Defines common TestNG groups.
 * 
 * @author ddidier
 */
public class TestNgGroups {

    /** A unit test. */
    public static final String UNIT = "UNIT";
    /** An integration test. */
    public static final String INTEGRATION = "INTEGRATION";
    /** A load test (find the breaking point). */
    public static final String LOAD = "LOAD";
    /** A performance test (compare between release). */
    public static final String PERFORMANCE = "PERFORMANCE";

    /** A smoke test. */
    public static final String SMOKE = "SMOKE";
    /** A slow test. */
    public static final String SLOW = "SLOW";

    /** May be extended. */
    protected TestNgGroups() {
        super();
    }
}
