package name.didier.david.test4j.testng;

import static name.didier.david.check4j.ConciseCheckers.checkNotNull;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;

import name.didier.david.test4j.unit.TestResourceHandler;

/**
 * Provide resource injection explicitly. See {@link TestNgResourceListener}.
 *
 * @author ddidier
 */
public class TestNgResources {

    /** Handle {@link name.didier.david.test4j.unit.TestResource}s. */
    private final TestResourceHandler handler;
    /** The current instance under test. */
    private final Object testInstance;
    /** The current method under test. */
    private final Method testMethod;

    /**
     * Default constructor.
     *
     * @param testInstance the instance of the running test.
     * @param testMethod the method of the running test.
     */
    public TestNgResources(Object testInstance, Method testMethod) {
        this.testInstance = checkNotNull(testInstance, "testInstance");
        this.testMethod = checkNotNull(testMethod, "testMethod");
        this.handler = new TestResourceHandler();
    }

    /**
     * Find the resource with the given pattern as a {@link File}, relative to the current test class and method.
     *
     * @param pattern the pattern to search for.
     * @return the file if found.
     * @throws name.didier.david.test4j.unit.TestResourceException if the file cannot be found.
     */
    public File asFile(String pattern) {
        return handler.findFile(pattern, testInstance, testMethod);
    }

    /**
     * Find the resource with the given pattern as a {@link URL}, relative to the current test class and method.
     *
     * @param pattern the pattern to search for.
     * @return the URL if found.
     * @throws name.didier.david.test4j.unit.TestResourceException if the URL cannot be found.
     */
    public URL asUrl(String pattern) {
        return handler.findUrl(pattern, testInstance, testMethod);
    }

    /**
     * Load the content of the resource with the given pattern, relative to the current test class and method.
     *
     * @param pattern the pattern to search for.
     * @return the content of the resource if found.
     * @throws name.didier.david.test4j.unit.TestResourceException if the resource cannot be found.
     */
    public String asContent(final String pattern) {
        return handler.loadContent(pattern, testInstance, testMethod);
    }

    /**
     * Interpolate the pattern as a relative path, relative to the current test class and method. See
     * {@link name.didier.david.test4j.unit.TestResource}.
     *
     * @param pattern the pattern to interpolate.
     * @return the interpolated pattern as a relative path.
     */
    public String toPath(final String pattern) {
        return handler.toPath(pattern, testInstance, testMethod);
    }
}
