package name.didier.david.test4j.testng;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.testng.annotations.DataProvider;

import name.didier.david.test4j.unit.DataProviders;

/**
 * Useful TestNG data providers for common testing patterns.
 *
 * <p>
 * Usage:
 * </p>
 *
 * <pre>
 *     import static name.didier.david.test4j.testng.TestNgDataProviders.BLANK_STRINGS;
 *
 *     &#064;Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
 *     public void test() { ... }
 * </pre>
 *
 * @author ddidier
 */
public final class TestNgDataProviders {

    /** Name of a data provider providing null strings. */
    public static final String NULL_STRINGS = "null_strings";
    /** Name of a data provider providing empty strings. */
    public static final String EMPTY_STRINGS = "empty_strings";
    /** Name of a data provider providing blank strings. */
    public static final String BLANK_STRINGS = "blank_strings";
    /** Name of a data provider providing empty but not null strings. */
    public static final String EMPTY_BUT_NOT_NULL_STRINGS = "empty_but_not_null_strings";

    /** Name of a data provider providing strictly negative numbers. */
    public static final String STRICTLY_NEGATIVE_NUMBERS = "strictly_negative_numbers";
    /** Name of a data provider providing negative numbers. */
    public static final String NEGATIVE_NUMBERS = "negative_numbers";
    /** Name of a data provider providing strictly positive numbers. */
    public static final String STRICTLY_POSITIVE_NUMBERS = "strictly_positive_numbers";
    /** Name of a data provider providing positive numbers. */
    public static final String POSITIVE_NUMBERS = "positive_numbers";

    /** Name of a data provider providing empty lists. */
    public static final String EMPTY_LISTS = "empty_lists";
    /** Name of a data provider providing empty sets. */
    public static final String EMPTY_SETS = "empty_sets";

    /** Name of a data provider providing non existing files. */
    public static final String NON_EXISTING_FILES = "non_existing_files";

    /** The HMAC key. */
    public static final String HMAC_KEY = "A very private key";
    /** Name of a data provider providing HMAC using MD5 encoded in base 16. */
    public static final String HMAC_MD5_BASE16 = "hmac_md5_base16";
    /** Name of a data provider providing HMAC using MD5 encoded in base 64. */
    public static final String HMAC_MD5_BASE64 = "hmac_md5_base64";
    /** Name of a data provider providing HMAC using SHA 256 encoded in base 16. */
    public static final String HMAC_SHA256_BASE16 = "hmac_sha256_base16";
    /** Name of a data provider providing HMAC using SHA 256 encoded in base 64. */
    public static final String HMAC_SHA256_BASE64 = "hmac_sha256_base64";

    /** Utility class. */
    private TestNgDataProviders() {
        super();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return {@code null} strings.
     */
    @DataProvider(name = NULL_STRINGS)
    public static String[][] nullStrings() {
        return DataProviders.nullStrings();
    }

    /**
     * @return {@code null} and empty strings.
     */
    @DataProvider(name = EMPTY_STRINGS)
    public static String[][] emptyStrings() {
        return DataProviders.emptyStrings();
    }

    /**
     * @return {@code null}, empty and blank strings.
     */
    @DataProvider(name = BLANK_STRINGS)
    public static String[][] blankStrings() {
        return DataProviders.blankStrings();
    }

    /**
     * @return empty and blank strings.
     */
    @DataProvider(name = EMPTY_BUT_NOT_NULL_STRINGS)
    public static String[][] emptyButNotNullStrings() {
        return DataProviders.emptyButNotNullStrings();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return strictly negative numbers.
     */
    @DataProvider(name = STRICTLY_NEGATIVE_NUMBERS)
    public static Number[][] strictlyNegativeNumbers() {
        return DataProviders.strictlyNegativeNumbers();
    }

    /**
     * @return negative or zero numbers.
     */
    @DataProvider(name = NEGATIVE_NUMBERS)
    public static Number[][] negativeNumbers() {
        return DataProviders.negativeNumbers();
    }

    /**
     * @return strictly positive numbers.
     */
    @DataProvider(name = STRICTLY_POSITIVE_NUMBERS)
    public static Number[][] strictlyPositiveNumbers() {
        return DataProviders.strictlyPositiveNumbers();
    }

    /**
     * @return positive or zero numbers.
     */
    @DataProvider(name = POSITIVE_NUMBERS)
    public static Number[][] positiveNumbers() {
        return DataProviders.positiveNumbers();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @param <T> the type of the elements in the list.
     * @return {@code null} and empty {@link List}.
     */
    @DataProvider(name = EMPTY_LISTS)
    public static <T> List<T>[][] emptyLists() {
        return DataProviders.emptyLists();
    }

    /**
     * @param <T> the type of the elements in the set.
     * @return {@code null} and empty {@link Set}.
     */
    @DataProvider(name = EMPTY_SETS)
    public static <T> Set<T>[][] emptySets() {
        return DataProviders.emptySets();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return non existing files: empty path, blank path, invalid path.
     */
    @DataProvider(name = NON_EXISTING_FILES)
    public static File[][] nonExistingFiles() {
        return DataProviders.nonExistingFiles();
    }

    // -----------------------------------------------------------------------------------------------------------------

    // Digests and conversions done with:
    // - http://www.freeformatter.com/hmac-generator.html
    // - http://tomeko.net/online_tools/hex_to_base64.php?lang=en

    /**
     * @return a pair message/signature (HMAC using MD5 encoded in hexadecimal).
     */
    @DataProvider(name = HMAC_MD5_BASE16)
    public static String[][] hmacMd5Base16() {
        return DataProviders.hmacMd5Base16();
    }

    /**
     * @return a pair message/signature (HMAC using MD5 encoded in base 64).
     */
    @DataProvider(name = HMAC_MD5_BASE64)
    public static String[][] hmacMd5Base64() {
        return DataProviders.hmacMd5Base64();
    }

    /**
     * @return a pair message/signature (HMAC using SHA256 encoded in hexadecimal).
     */
    @DataProvider(name = HMAC_SHA256_BASE16)
    public static String[][] hmacSha256Base16() {
        return DataProviders.hmacSha256Base16();
    }

    /**
     * @return a pair message/signature (HMAC using SHA256 encoded in base 64).
     */
    @DataProvider(name = HMAC_SHA256_BASE64)
    public static String[][] hmacSha256Base64() {
        return DataProviders.hmacSha256Base64();
    }
}
