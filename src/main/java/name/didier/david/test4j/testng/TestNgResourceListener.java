package name.didier.david.test4j.testng;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import name.didier.david.test4j.unit.TestResourceException;
import name.didier.david.test4j.unit.TestResourceHandler;

/**
 * Provide resource injection through {@link name.didier.david.test4j.unit.TestResource} annotations and
 * {@link TestNgResources} fields.
 *
 * <p>
 * The pattern is a custom string which may use the following placeholders:
 * </p>
 *
 * <ul>
 * <li><code>{c}</code> is the name of the current test class, given by {@link Class#getSimpleName()}, e.g.
 * <code>MyTestCase</code> or <code>MyTestCase-MyInnerClass</code></li>
 * <li><code>{f}</code> is the name of the annotated field, given by {@link java.lang.reflect.Field#getName()}</li>
 * <li><code>{m}</code> is the name of the current test method, given by {@link java.lang.reflect.Method#getName()}</li>
 * <li><code>{p}</code> is the package name of the current test class as a path, given by {@link Package#getName()},
 * e.g. <code>name/didier/david/test4j/testng</code></li>
 * </ul>
 *
 * <p>
 * Usage:
 * </p>
 *
 * <pre>
   package name.didier.david.test4j.testng;

   &#64;Listeners(TestNgResourceListener.class)
   public class TestNgResourceListenerTest {

     // resolves to name/didier/david/test4j/testng/TestNgResourceListenerTest-data/&lt;method_name&gt;
     // with &lt;method_name&gt; the name ot the current running test method, e.g. "test"
     &#64;TestResource("/{p}/{c}-data/{m}")
     private File fileField;

     // resolves to the content of name/didier/david/test4j/testng/TestNgResourceListenerTest-data/&lt;method_name&gt;
     // with &lt;method_name&gt; the name ot the current running test method, e.g. "test"
     &#64;TestResource("/{p}/{c}-data/{m}")
     private String stringField;

     // will hold an instance of TestNgResources correctly instanciated.
     private TestNgResources resources;

     &#64;Test
     public void test() {
       // resolves to name/didier/david/test4j/testng/TestNgResourceListenerTest-data/test
       File file = testResource.asFile("/{p}/{c}-data/{m}");
       // resolves to the content of name/didier/david/test4j/junit/TestResourceRuleTest-data/test
       String fileContent = testResource.asContent("/{p}/{c}-data/{m}");
       ...
     }
   }
 * </pre>
 *
 * @author ddidier
 */
public class TestNgResourceListener
        implements IInvokedMethodListener {

    /** Handle {@link name.didier.david.test4j.unit.TestResource}s. */
    private final TestResourceHandler handler;
    /** The current instance under test. */
    private Object testInstance;
    /** The current method under test. */
    private Method testMethod;

    /** Default constructor. */
    public TestNgResourceListener() {
        handler = new TestResourceHandler();
    }

    @Override
    public void beforeInvocation(final IInvokedMethod method, final ITestResult testResult) {
        testInstance = method.getTestMethod().getInstance();
        testMethod = method.getTestMethod().getConstructorOrMethod().getMethod();

        if (isListening()) {
            handler.inject(testInstance, testMethod);
            injectTestResources(testInstance, testMethod);
        }
    }

    @Override
    public void afterInvocation(final IInvokedMethod method, final ITestResult testResult) {
        if (isListening()) {
            handler.clear(testInstance, testMethod);
            clearTestResources(testInstance, testMethod);
        }

        // testInstance = null;
        // testMethod = null;
    }

    /**
     * @return <code>true</code> if the class of the specified test instance or one of its superclass is annotated with
     *         {@link org.testng.annotations.Listeners} referencing {@link TestNgResourceListener}, <code>false</code>
     *         otherwise.
     */
    protected boolean isListening() {
        return TestNgListenerUtils.isListening(testInstance, TestNgResourceListener.class);
    }

    /**
     * Inject the {@link TestResources} of the given instance for the given test.
     *
     * @param instance the instance of the running test.
     * @param method the method of the running test.
     */
    private void injectTestResources(final Object instance, final Method method) {
        try {
            for (Field testField : instance.getClass().getDeclaredFields()) {
                if (TestNgResources.class.isAssignableFrom(testField.getType())) {
                    testField.setAccessible(true);
                    testField.set(instance, new TestNgResources(instance, method));
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new TestResourceException("Error while setting TestNgResources", e, testInstance, testMethod);
        }
    }

    /**
     * Clear the {@link TestResources} of the given instance for the given test.
     *
     * @param instance the instance of the running test.
     * @param method the method of the running test.
     */
    private void clearTestResources(final Object instance, final Method method) {
        try {
            for (Field testField : instance.getClass().getDeclaredFields()) {
                if (TestNgResources.class.isAssignableFrom(testField.getType())) {
                    testField.setAccessible(true);
                    testField.set(instance, null);
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new TestResourceException("Error while clearing TestNgResources", e, testInstance, testMethod);
        }
    }
}
