package name.didier.david.test4j.testng;

import org.testng.ITestNGListener;
import org.testng.annotations.Listeners;

/**
 * Utilities dealing with {@link org.testng.ITestNGListener}s.
 *
 * @author ddidier
 */
public final class TestNgListenerUtils {

    /** Utility class. */
    private TestNgListenerUtils() {
        super();
    }

    /**
     * Returns <code>true</code> if the specified test class or one of its superclass is annotated with
     * {@link Listeners} referencing at least a specified listener class, <code>false</code> otherwise. Because any
     * {@link Listeners} annotation will apply to the entire suite and not only to the annotated class.
     *
     * @param testClass the class being tested for the {@link Listeners} annotation.
     * @param listenerClasses the listener classes to look for.
     * @return <code>true</code> if the specified test class or one of its superclass is annotated with
     *         {@link Listeners} referencing at least a specified listener class, <code>false</code> otherwise.
     */
    @SuppressWarnings("unchecked")
    public static boolean isListening(Class<?> testClass, Class<? extends ITestNGListener>... listenerClasses) {
        // CSOFF: ReturnCountCheck -2

        Listeners listenerAnnotation = testClass.getAnnotation(Listeners.class);
        if (listenerAnnotation != null) {
            for (Class<? extends ITestNGListener> actualListenerClass : listenerAnnotation.value()) {
                for (Class<? extends ITestNGListener> listenerClass : listenerClasses) {
                    if (listenerClass.isAssignableFrom(actualListenerClass)) {
                        return true;
                    }
                }
            }
        }

        if (testClass.getSuperclass() != null) {
            return isListening(testClass.getSuperclass(), listenerClasses);
        }

        return false;
    }

    /**
     * Returns <code>true</code> if the class of the specified test instance or one of its superclass is annotated with
     * {@link Listeners} referencing at least a specified listener class, <code>false</code> otherwise. Because any
     * {@link Listeners} annotation will apply to the entire suite and not only to the annotated class.
     *
     * @param testInstance the instance of the class being tested for the {@link Listeners} annotation.
     * @param listenerClasses the listener classes to look for.
     * @return <code>true</code> if the specified test class or one of its superclass is annotated with
     *         {@link Listeners} referencing at least a specified listener class, <code>false</code> otherwise.
     */
    @SuppressWarnings("unchecked")
    public static boolean isListening(Object testInstance, Class<? extends ITestNGListener>... listenerClasses) {
        return isListening(testInstance.getClass(), listenerClasses);
    }

    /**
     * Returns <code>true</code> if the specified test class or one of its superclass is annotated with
     * {@link Listeners} referencing the specified listener class, <code>false</code> otherwise. Because any
     * {@link Listeners} annotation will apply to the entire suite and not only to the annotated class.
     *
     * @param testClass the class being tested for the {@link Listeners} annotation.
     * @param listenerClass the listener class to look for.
     * @return <code>true</code> if the specified test class or one of its superclass is annotated with
     *         {@link Listeners} referencing the specified listener class, <code>false</code> otherwise.
     */
    @SuppressWarnings("unchecked")
    public static boolean isListening(Class<?> testClass, Class<? extends ITestNGListener> listenerClass) {
        return isListening(testClass, new Class[] { listenerClass });
    }

    /**
     * Returns <code>true</code> if the class of the specified test instance or one of its superclass is annotated with
     * {@link Listeners} referencing the specified listener class, <code>false</code> otherwise. Because any
     * {@link Listeners} annotation will apply to the entire suite and not only to the annotated class.
     *
     * @param testInstance the instance of the class being tested for the {@link Listeners} annotation.
     * @param listenerClass the listener class to look for.
     * @return <code>true</code> if the class of the specified test instance or one of its superclass is annotated with
     *         {@link Listeners} referencing the specified listener class, <code>false</code> otherwise.
     */
    public static boolean isListening(Object testInstance, Class<? extends ITestNGListener> listenerClass) {
        return isListening(testInstance.getClass(), listenerClass);
    }
}
