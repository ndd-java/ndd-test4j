package name.didier.david.test4j.unit;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * A strategy to compute the name candidates of a {@link TestResource}.
 *
 * @author ddidier
 */
public interface TestResourceNamingStrategy {

    /**
     * Returns name to look for.
     *
     * @param pattern a pattern (see {@link TestResource#value()}).
     * @param testClass the associated test class.
     * @param testField the associated test field.
     * @param testMethod the associated test method.
     * @return the name to look for.
     */
    String resourceName(String pattern, Class<?> testClass, Field testField, Method testMethod);

    /**
     * Returns the ordered list of names to look for.
     *
     * @param patterns a list of patterns (see {@link TestResource#value()}).
     * @param testClass the associated test class.
     * @param testField the associated test field.
     * @param testMethod the associated test method.
     * @return the ordered list of names to look for.
     */
    List<String> resourceNames(String[] patterns, Class<?> testClass, Field testField, Method testMethod);
}
