package name.didier.david.test4j.unit;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.check4j.ConciseCheckers.checkNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ClassUtils;

/**
 * A strategy to compute the name candidates of a {@link TestResource}. See {@link TestResource#value()}.
 *
 * @author ddidier
 */
public class DefaultTestResourceNamingStrategy
        implements TestResourceNamingStrategy {

    /** The default pattern {@value DefaultTestResourceNamingStrategy#DEFAULT_PATTERN}. */
    public static final String DEFAULT_PATTERN = "/{p}/{c}/{m}/{f}";

    /**
     * Default constructor.
     */
    public DefaultTestResourceNamingStrategy() {
        super();
    }

    @Override
    public String resourceName(String pattern, Class<?> testClass, Field testField, Method testMethod) {
        String interpolated = checkNotNull(pattern, "pattern");

        if ("".equals(interpolated)) {
            interpolated = DEFAULT_PATTERN;
        }

        if (testClass != null) {
            interpolated = interpolated.replace("{c}", ClassUtils.getShortClassName(testClass).replace('.', '-'));

            if (testClass.getPackage() != null && testClass.getPackage().getName() != null) {
                interpolated = interpolated.replace("{p}", testClass.getPackage().getName().replace('.', '/'));
            }
        }

        if (testField != null) {
            interpolated = interpolated.replace("{f}", testField.getName());
        }

        if (testMethod != null) {
            interpolated = interpolated.replace("{m}", testMethod.getName());
        }

        return interpolated;
    }

    @Override
    public List<String> resourceNames(String[] patterns, Class<?> testClass, Field testField, Method testMethod) {
        checkNotNull(patterns, "patterns");

        if (patterns.length == 0) {
            String resourceName = resourceName(DEFAULT_PATTERN, testClass, testField, testMethod);
            return newArrayList(resourceName);
        }

        List<String> resourceNames = new ArrayList<>();
        for (String pattern : patterns) {
            resourceNames.add(resourceName(pattern, testClass, testField, testMethod));
        }
        return resourceNames;
    }
}
