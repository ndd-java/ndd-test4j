package name.didier.david.test4j.unit;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Represent a resource to be injected in a field of a TestNG class.
 *
 * <p>
 * The value is actually a pattern which is a custom string which may use the following placeholders:
 * </p>
 *
 * <ul>
 * <li><code>{c}</code> is the name of the current test class, given by {@link Class#getSimpleName()}, e.g.
 * <code>MyTestCase</code> or <code>MyTestCase-MyInnerClass</code></li>
 * <li><code>{f}</code> is the name of the annotated field, given by {@link java.lang.reflect.Field#getName()}</li>
 * <li><code>{m}</code> is the name of the current test method, given by {@link java.lang.reflect.Method#getName()}</li>
 * <li><code>{p}</code> is the package name of the current test class as a path, given by {@link Package#getName()},
 * e.g. <code>name/didier/david/test4j/unit</code></li>
 * </ul>
 *
 * @author ddidier
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface TestResource {

    /** The string representing the class name. */
    String CLASS_NAME = "{c}";
    /** The string representing the field name. */
    String FIELD_NAME = "{f}";
    /** The string representing the method name. */
    String METHOD_NAME = "{m}";
    /** The string representing the package name. */
    String PACKAGE_NAME = "{p}";

    /**
     * A list of patterns which may use of the following interpolation strings: {@value TestResource#CLASS_NAME},
     * {@value TestResource#FIELD_NAME}, {@value TestResource#METHOD_NAME} or {@value TestResource#PACKAGE_NAME}.
     *
     * @return the patterns of the resource file name.
     */
    String[] value() default "";

    /**
     * @return <code>true</code> if the resource may <code>null</code>, <code>false</code> otherwise.
     */
    boolean optional() default false;
}
