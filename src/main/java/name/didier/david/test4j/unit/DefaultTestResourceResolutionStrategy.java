package name.didier.david.test4j.unit;

import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A strategy to resolve a {@link TestResource} using the classpath. See {@link Class#getResource(String)}.
 *
 * @author ddidier
 */
public class DefaultTestResourceResolutionStrategy
        implements TestResourceResolutionStrategy {

    /** The associated logger. */
    private static final Logger logger = LoggerFactory.getLogger(DefaultTestResourceResolutionStrategy.class);

    /**
     * Default constructor.
     */
    public DefaultTestResourceResolutionStrategy() {
        super();
    }

    @Override
    public URL findResource(Class<?> testClass, String resourceName) {
        logger.trace("Resolving test resource from classpath: {}", resourceName);
        URL result = testClass.getResource(resourceName);

        if (result != null) {
            logger.trace("Test resource found in classpath: {}", resourceName);
            return result;
        }

        logger.trace("Test resource not found in classpath: {}", resourceName);
        return null;
    }

    @Override
    public URL findResource(final Class<?> testClass, final List<String> resourceNames) {
        for (String resourceName : resourceNames) {
            URL result = findResource(testClass, resourceName);

            if (result != null) {
                return result;
            }
        }

        logger.trace("Test resource not found in classpath");
        return null;
    }
}
