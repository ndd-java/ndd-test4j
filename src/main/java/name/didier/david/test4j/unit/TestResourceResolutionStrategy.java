package name.didier.david.test4j.unit;

import java.net.URL;
import java.util.List;

/**
 * A strategy to resolve a {@link TestResource}.
 *
 * @author ddidier
 */
public interface TestResourceResolutionStrategy {

    /**
     * Returns the URL of the specified resource, {@code null} if not found.
     *
     * @param testClass the test class.
     * @param resourceName the resource name candidate.
     * @return the URL of the specified resource, {@code null} if not found.
     */
    URL findResource(Class<?> testClass, String resourceName);

    /**
     * Returns the URL of the specified resource, {@code null} if not found.
     *
     * @param testClass the test class.
     * @param resourceNames the ordered resource name candidates.
     * @return the URL of the specified resource, {@code null} if not found.
     */
    URL findResource(Class<?> testClass, List<String> resourceNames);
}
