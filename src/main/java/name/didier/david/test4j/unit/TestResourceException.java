package name.didier.david.test4j.unit;

import static name.didier.david.check4j.ConciseCheckers.checkNotNull;

import java.lang.reflect.Method;

/**
 * Raised when trying to resolve a {@link TestResource}.
 *
 * @author ddidier
 */
public class TestResourceException
        extends RuntimeException {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The instance of the test associated with the {@link TestResource} raising the exception. */
    private final Object testInstance;
    /** The method of the test associated with the {@link TestResource} raising the exception. */
    private final Method testMethod;

    /**
     * @param testInstance the instance of the test associated with the {@link TestResource} raising the exception.
     * @param testMethod the method of the test associated with the {@link TestResource} raising the exception.
     */
    public TestResourceException(Object testInstance, Method testMethod) {
        this(null, null, testInstance, testMethod);
    }

    /**
     * @param message the message.
     * @param testInstance the instance of the test associated with the {@link TestResource} raising the exception.
     * @param testMethod the method of the test associated with the {@link TestResource} raising the exception.
     */
    public TestResourceException(String message, Object testInstance, Method testMethod) {
        this(message, null, testInstance, testMethod);
    }

    /**
     * @param cause the cause.
     * @param testInstance the instance of the test associated with the {@link TestResource} raising the exception.
     * @param testMethod the method of the test associated with the {@link TestResource} raising the exception.
     */
    public TestResourceException(Throwable cause, Object testInstance, Method testMethod) {
        this(null, cause, testInstance, testMethod);
    }

    /**
     * @param message the message.
     * @param cause the cause.
     * @param testInstance the instance of the test associated with the {@link TestResource} raising the exception.
     * @param testMethod the method of the test associated with the {@link TestResource} raising the exception.
     */
    public TestResourceException(String message, Throwable cause, Object testInstance, Method testMethod) {
        super(message, cause);
        this.testInstance = checkNotNull(testInstance, "testInstance");
        this.testMethod = checkNotNull(testMethod, "testMethod");
    }

    /**
     * @return the instance of the test associated with the {@link TestResource} raising the exception.
     */
    public Object getTestInstance() {
        return testInstance;
    }

    /**
     * @return the method of the test associated with the {@link TestResource} raising the exception.
     */
    public Method getTestMethod() {
        return testMethod;
    }

}
