package name.didier.david.test4j.utils;

import static com.google.common.base.Throwables.throwIfUnchecked;

/**
 * A little framework to capture then test exit calls. Usage:
 *
 * <pre>
 * new ExitCaptureJob() {
 *     &#064;Override
 *     protected void execute() {
 *         System.exit(-1);
 *     }
 *
 *     &#064;Override
 *     protected void checkStatus(int status) {
 *         assertThat(status).isEqualTo(-1);
 *     }
 * }.run();
 * </pre>
 *
 * @author ddidier
 */
public abstract class ExitCaptureJob
        implements Runnable {

    /** The exit captor. */
    private final ExitCaptor captor;

    /** Default constructor. */
    public ExitCaptureJob() {
        super();
        this.captor = new ExitCaptor();
    }

    @Override
    public void run() {
        try {
            startCapturing();
            execute();
        } catch (NoExitException e) {
            checkStatus(e.getStatus());
        } catch (Exception e) {
            // OK to catch Exception here
            throwIfUnchecked(e);
            throw new RuntimeException(e);
        } finally {
            stopCapturing();
        }
    }

    /**
     * Start capturing exit calls.
     */
    protected void startCapturing() {
        captor.startCapturing();
    }

    /**
     * Execute whatever you want to capture.
     */
    protected abstract void execute();

    /**
     * Stop capturing exit calls.
     */
    protected void stopCapturing() {
        captor.stopCapturing();
    }

    /**
     * Check the captured exit call status.
     *
     * @param status the exit status.
     */
    protected abstract void checkStatus(int status);
}
