package name.didier.david.test4j.utils;

/**
 * Raised when trying to exit an application running with a {@link NoExitSecurityManager}.
 * 
 * @author ddidier
 */
public class NoExitException
        extends SecurityException {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The exit status. */
    private final int status;

    /**
     * Constructor.
     * 
     * @param message the message.
     * @param status the exit status.
     */
    public NoExitException(final String message, final int status) {
        super(message);
        this.status = status;
    }

    /**
     * @return the exit status.
     */
    public int getStatus() {
        return status;
    }
}
