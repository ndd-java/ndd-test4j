package name.didier.david.test4j.utils;

import static com.google.common.base.Throwables.throwIfUnchecked;

/**
 * A little framework to capture then test the standard output stream and the standard error stream. Usage:
 *
 * <pre>
 * new StdCaptureJob() {
 *     &#064;Override
 *     protected void execute() {
 *         // CSOFF: RegexpCheck +2
 *         System.out.println(&quot;toStdOut&quot;);
 *         System.err.println(&quot;toStdErr&quot;);
 *     }
 *
 *     &#064;Override
 *     protected void checkCaptures(String stdOutCapture, String stdErrCapture) {
 *         assertThat(stdOutCapture).isEqualTo(&quot;toStdOut&quot;);
 *         assertThat(stdErrCapture).isEqualTo(&quot;toStdErr&quot;);
 *     }
 * }.run();
 * </pre>
 *
 * @author ddidier
 */
public abstract class StdCaptureJob
        implements Runnable {

    /** The stream captor. */
    private final StdCaptor captor;
    /** Flag to print the captured STDOUT. */
    private final boolean printStdErr;
    /** Flag to print the captured STDERR. */
    private final boolean printStdOut;

    /** Default constructor. */
    public StdCaptureJob() {
        this(false, false);
    }

    /**
     * Default constructor.
     *
     * @param printStdOut <code>true</code> to print the captured STDOUT, <code>false</code> otherwise.
     * @param printStdErr <code>true</code> to print the captured STDERR, <code>false</code> otherwise.
     */
    public StdCaptureJob(final boolean printStdOut, final boolean printStdErr) {
        super();
        this.captor = new StdCaptor();
        this.printStdOut = printStdOut;
        this.printStdErr = printStdErr;
    }

    @Override
    public void run() {
        try {
            startCapturing();
            execute();
        } catch (Exception e) {
            // OK to catch Exception here
            throwIfUnchecked(e);
            throw new RuntimeException(e);
        } finally {
            stopCapturing();
        }
    }

    /**
     * Start capturing STDOUT and STDERR.
     */
    protected void startCapturing() {
        captor.startStdOutCapture();
        captor.startStdErrCapture();
    }

    /**
     * Execute whatever you want to capture.
     */
    protected abstract void execute();

    /**
     * Stop capturing STDOUT and STDERR then call {@link #checkCaptures(String, String)}.
     */
    protected void stopCapturing() {
        String stdOutCapture = captor.stopStdOutCapture(printStdOut);
        String stdErrCapture = captor.stopStdErrCapture(printStdErr);
        checkCaptures(stdOutCapture, stdErrCapture);
    }

    /**
     * Check the captured streams.
     *
     * @param stdOutCapture the captured STDOUT.
     * @param stdErrCapture the captured STDERR.
     */
    protected abstract void checkCaptures(String stdOutCapture, String stdErrCapture);
}
