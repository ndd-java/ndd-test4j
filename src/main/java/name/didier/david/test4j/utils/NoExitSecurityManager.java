package name.didier.david.test4j.utils;

import java.security.Permission;

/**
 * Forbid to exit the application.
 * 
 * @author ddidier
 */
public class NoExitSecurityManager
        extends SecurityManager {

    /** Default constructor. */
    public NoExitSecurityManager() {
        super();
    }

    /**
     * Allow anything.
     * 
     * @param perm the requested permission.
     */
    @Override
    public void checkPermission(final Permission perm) {
        // allow anything.
    }

    /**
     * Allow anything.
     * 
     * @param perm the requested permission.
     * @param context a system-dependent security context.
     */
    @Override
    public void checkPermission(final Permission perm, final Object context) {
        // allow anything.
    }

    /**
     * Throws a <code>NoExitException</code>.
     * 
     * @param status the exit status.
     */
    @Override
    public void checkExit(final int status) {
        super.checkExit(status);
        throw new NoExitException("Intercepted exit with status " + status, status);
    }
}
