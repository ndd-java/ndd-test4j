package name.didier.david.test4j.assertions;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.lang.reflect.InvocationTargetException;

import org.testng.annotations.Test;

@Test
public class CodingStandardClassAssertTest {

    private static final String CLASS_NAME = "name.didier.david.test4j.assertions.CodingStandardClassAssertTest";
    private static final String INNER_CLASS_SEPARATOR = "$";

    @Test
    public static class describe_isUtility {

        private static final String D_CLASS_NAME = CLASS_NAME + "$describe_isUtility";

        public void should_fail_if_the_class_is_null() {
            try {
                new CodingStandardClassAssert(null).isUtility();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage("\nExpecting actual not to be null");
            }
        }

        public void should_fail_if_the_class_is_not_final() {
            try {
                new CodingStandardClassAssert(MyNonFinalClass.class).isUtility();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(expectingClass(MyNonFinalClass.class, "to be a final class."));
            }
        }

        public void should_fail_if_the_class_has_two_constructors() {
            try {
                new CodingStandardClassAssert(MyClassWithTwoConstructors.class).isUtility();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(
                        expectedClassTo(MyClassWithTwoConstructors.class, "have only one constructor but had 2"));
            }
        }

        public void should_fail_if_the_class_has_a_constructor_which_is_not_private() {
            // @formatter:off
            Class<?>[] classes = new Class[] {
                MyClassWithOnePublicConstructor.class,
                MyClassWithOneProtectedConstructor.class,
                MyClassWithOnePackagePrivateConstructor.class,
            };
            // @formatter:on

            for (Class<?> clazz : classes) {
                try {
                    new CodingStandardClassAssert(clazz).isUtility();
                    failBecauseExceptionWasNotThrown(AssertionError.class);
                } catch (AssertionError e) {
                    assertThat(e).hasMessage(expectedClassTo(clazz, "have a single private constructor"));
                }
            }
        }

        public void should_fail_if_the_constructor_raise_an_exception() {
            try {
                new CodingStandardClassAssert(MyClassWithFailingConstructor.class).isUtility();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(expectedClassTo(MyClassWithFailingConstructor.class,
                        "have a usable constructor but it raised %s", InvocationTargetException.class.getName()));
            }
        }

        public void should_fail_if_the_class_has_an_instance_method() {
            try {
                new CodingStandardClassAssert(MyClassWithAnInstanceMethod.class).isUtility();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(expectedClassTo(MyClassWithAnInstanceMethod.class,
                        "have only static methods but has 'notStatic'"));
            }
        }

        public void should_not_fail_if_the_class_follows_coding_standards() {
            new CodingStandardClassAssert(MyUtilityClass.class).isUtility();
        }

        // ------------------------------

        /** Messages from AssertJ built-in assertions */
        private static String expectingClass(final Class<?> clazz, final String format, final Object... arguments) {
            String className = D_CLASS_NAME + INNER_CLASS_SEPARATOR + clazz.getSimpleName();
            className = className.replace('$', '.');
            return format("\nExpecting:\n  <%s>\n%s", className, format(format, arguments));
        }

        /** Messages from NDD Test4J assertions */
        private static String expectedClassTo(final Class<?> clazz, final String format, final Object... arguments) {
            String className = D_CLASS_NAME + INNER_CLASS_SEPARATOR + clazz.getSimpleName();
            return format("Expected class %s to %s", className, format(format, arguments));
        }

        // ------------------------------

        // CSOFF: FinalClassCheck
        private static class MyNonFinalClass {
            private MyNonFinalClass() {
                super();
            }
        }

        private static final class MyClassWithTwoConstructors {
            private MyClassWithTwoConstructors() {
                super();
            }

            @SuppressWarnings("unused")
            private MyClassWithTwoConstructors(final int p) {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static final class MyClassWithOnePublicConstructor {
            protected MyClassWithOnePublicConstructor() {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static final class MyClassWithOneProtectedConstructor {
            protected MyClassWithOneProtectedConstructor() {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static final class MyClassWithOnePackagePrivateConstructor {
            MyClassWithOnePackagePrivateConstructor() {
                super();
            }
        }

        private static final class MyClassWithFailingConstructor {
            private MyClassWithFailingConstructor() {
                throw new RuntimeException("failed");
            }
        }

        @SuppressWarnings("unused")
        private static final class MyClassWithAnInstanceMethod {
            private MyClassWithAnInstanceMethod() {
                super();
            }

            public void notStatic() {
                // empty
            }
        }

        @SuppressWarnings("unused")
        private static final class MyUtilityClass {
            private MyUtilityClass() {
                super();
            }

            public static void notStatic() {
                // empty
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_isAbstract {

        private static final String D_CLASS_NAME = CLASS_NAME + "$describe_isAbstract";

        public void should_fail_if_the_class_is_null() {
            try {
                new CodingStandardClassAssert(null).isAbstract();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage("\nExpecting actual not to be null");
            }
        }

        public void should_fail_if_the_class_is_final() {
            try {
                new CodingStandardClassAssert(MyFinalClass.class).isAbstract();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(expectingClass(MyFinalClass.class, "not to be a final class."));
            }
        }

        public void should_fail_if_the_class_has_a_public_constructor() {
            try {
                new CodingStandardClassAssert(MyClassWithPublicConstructor.class).isAbstract();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(
                        expectedClassTo(MyClassWithPublicConstructor.class, "not have a public constructor"));
            }
        }

        public void should_fail_if_the_class_has_not_a_protected_constructor() {
            // @formatter:off
            Class<?>[] classes = new Class[] {
                MyClassWithPackagePrivateConstructor.class,
                MyClassWithPrivateConstructor.class,
            };
            // @formatter:on

            for (Class<?> clazz : classes) {
                try {
                    new CodingStandardClassAssert(clazz).isAbstract();
                    failBecauseExceptionWasNotThrown(AssertionError.class);
                } catch (AssertionError e) {
                    assertThat(e).hasMessage(expectedClassTo(clazz, "have a protected constructor"));
                }
            }
        }

        public void should_not_fail_if_the_class_follows_coding_standards() {
            new CodingStandardClassAssert(MyAbstractClass.class).isAbstract();
        }

        // ------------------------------

        /** Messages from AssertJ built-in assertions */
        private static String expectingClass(final Class<?> clazz, final String format, final Object... arguments) {
            String className = D_CLASS_NAME + INNER_CLASS_SEPARATOR + clazz.getSimpleName();
            className = className.replace('$', '.');
            return format("\nExpecting:\n  <%s>\n%s", className, format(format, arguments));
        }

        /** Messages from NDD Test4J assertions */
        private static String expectedClassTo(final Class<?> clazz, final String format, final Object... arguments) {
            String className = D_CLASS_NAME + INNER_CLASS_SEPARATOR + clazz.getSimpleName();
            return format("Expected class %s to %s", className, format(format, arguments));
        }

        // ------------------------------

        private static final class MyFinalClass {
            private MyFinalClass() {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static class MyClassWithPublicConstructor {
            // CSOFF: RedundantModifierCheck
            public MyClassWithPublicConstructor() {
                super();
            }
        }

        // CSOFF: FinalClassCheck
        private static class MyClassWithPackagePrivateConstructor {
            private MyClassWithPackagePrivateConstructor() {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static class MyClassWithPrivateConstructor {
            MyClassWithPrivateConstructor() {
                super();
            }
        }

        @SuppressWarnings("unused")
        private static class MyAbstractClass {
            protected MyAbstractClass() {
                super();
            }

            private MyAbstractClass(final int i) {
                super();
            }

            MyAbstractClass(final long i) {
                super();
            }
        }
    }
}
