package name.didier.david.test4j.assertions.jaxrs;

import static java.lang.String.format;

import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.BAD_GATEWAY;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.EXPECTATION_FAILED;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.FOUND;
import static javax.ws.rs.core.Response.Status.GATEWAY_TIMEOUT;
import static javax.ws.rs.core.Response.Status.GONE;
import static javax.ws.rs.core.Response.Status.HTTP_VERSION_NOT_SUPPORTED;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.LENGTH_REQUIRED;
import static javax.ws.rs.core.Response.Status.METHOD_NOT_ALLOWED;
import static javax.ws.rs.core.Response.Status.MOVED_PERMANENTLY;
import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NOT_IMPLEMENTED;
import static javax.ws.rs.core.Response.Status.NOT_MODIFIED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.PARTIAL_CONTENT;
import static javax.ws.rs.core.Response.Status.PAYMENT_REQUIRED;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static javax.ws.rs.core.Response.Status.PROXY_AUTHENTICATION_REQUIRED;
import static javax.ws.rs.core.Response.Status.REQUESTED_RANGE_NOT_SATISFIABLE;
import static javax.ws.rs.core.Response.Status.REQUEST_ENTITY_TOO_LARGE;
import static javax.ws.rs.core.Response.Status.REQUEST_TIMEOUT;
import static javax.ws.rs.core.Response.Status.REQUEST_URI_TOO_LONG;
import static javax.ws.rs.core.Response.Status.RESET_CONTENT;
import static javax.ws.rs.core.Response.Status.SEE_OTHER;
import static javax.ws.rs.core.Response.Status.SERVICE_UNAVAILABLE;
import static javax.ws.rs.core.Response.Status.TEMPORARY_REDIRECT;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static javax.ws.rs.core.Response.Status.UNSUPPORTED_MEDIA_TYPE;
import static javax.ws.rs.core.Response.Status.USE_PROXY;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static name.didier.david.test4j.assertions.jaxrs.ResponseAssert.assertThat;
import static name.didier.david.test4j.utils.JsonUtils.json;
import static name.didier.david.test4j.utils.JsonUtils.normalizeJson;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.ComparisonFailure;
import org.testng.annotations.Test;

@Test
public class ResponseAssertTest {

    @Test
    public static class describe_hasJsonContent {

        public void returns_the_assertion_with_normalized_and_equal_json_strings() {
            String actualJson = normalizeJson("{'id': 123, 'name': 'a cool name'}");
            String expectedJson = actualJson;

            Response response = mock(Response.class);
            when(response.readEntity(String.class)).thenReturn(actualJson);

            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasJsonContent(expectedJson);
            assertThat(returnValue).isSameAs(assertion);
        }

        public void raises_an_AssertionError_with_normalized_and_different_json_strings() {
            String actualJson = normalizeJson("{'id': 123, 'name': 'a cool name'}");
            String expectedJson = normalizeJson("{'id': 123, 'name': 'a not so cool name'}");

            Response response = mock(Response.class);
            when(response.readEntity(String.class)).thenReturn(actualJson);

            try {
                assertThat(response).hasJsonContent(expectedJson);
                failBecauseExceptionWasNotThrown(ComparisonFailure.class);
            } catch (ComparisonFailure e) {
                assertThat(e).hasMessage("expected:<...{\"id\":123,\"name\":\"a [not so ]cool name\"}\"> "
                        + "but was:<...{\"id\":123,\"name\":\"a []cool name\"}\">");
            }
        }

        public void returns_the_assertion_with_not_normalized_but_equal_json_strings() {
            String actualJson = json("{'id'   :   123, 'name': 'a cool name'}");
            String expectedJson = json("{'id': 123, 'name'   :   'a cool name'}");

            Response response = mock(Response.class);
            when(response.readEntity(String.class)).thenReturn(actualJson);

            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasJsonContent(expectedJson);
            assertThat(returnValue).isSameAs(assertion);
        }

        public void raises_an_AssertionError_with_not_normalized_and_different_json_strings() {
            String actualJson = json("{'id'   :   123, 'name': 'a cool name'}");
            String expectedJson = json("{'id': 123, 'name'   :   'a not so cool name'}");

            Response response = mock(Response.class);
            when(response.readEntity(String.class)).thenReturn(actualJson);

            try {
                assertThat(response).hasJsonContent(expectedJson);
                failBecauseExceptionWasNotThrown(ComparisonFailure.class);
            } catch (ComparisonFailure e) {
                assertThat(e).hasMessage("expected:<...{\"id\":123,\"name\":\"a [not so ]cool name\"}\"> "
                        + "but was:<...{\"id\":123,\"name\":\"a []cool name\"}\">");
            }
        }

        public void returns_the_assertion_with_not_normalized_but_equal_json_elements() {
            String actualJson = json("{'id'   :   123, 'name': 'a cool name'}");
            String[] expectedJson = { "{", "  'id': 123,", "  'name': 'a cool name'", "}" };

            Response response = mock(Response.class);
            when(response.readEntity(String.class)).thenReturn(actualJson);

            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasJsonContent(expectedJson);
            assertThat(returnValue).isSameAs(assertion);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // generated:
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_hasStatus {

        public void hasStatusOk() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (OK.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusOk();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusOk();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(OK, status));
                    }
                }
            }
        }

        public void hasStatusCreated() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (CREATED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusCreated();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusCreated();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(CREATED, status));
                    }
                }
            }
        }

        public void hasStatusAccepted() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (ACCEPTED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusAccepted();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusAccepted();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(ACCEPTED, status));
                    }
                }
            }
        }

        public void hasStatusNoContent() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (NO_CONTENT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusNoContent();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusNoContent();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(NO_CONTENT, status));
                    }
                }
            }
        }

        public void hasStatusResetContent() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (RESET_CONTENT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusResetContent();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusResetContent();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(RESET_CONTENT, status));
                    }
                }
            }
        }

        public void hasStatusPartialContent() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (PARTIAL_CONTENT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusPartialContent();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusPartialContent();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(PARTIAL_CONTENT, status));
                    }
                }
            }
        }

        public void hasStatusMovedPermanently() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (MOVED_PERMANENTLY.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusMovedPermanently();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusMovedPermanently();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(MOVED_PERMANENTLY, status));
                    }
                }
            }
        }

        public void hasStatusFound() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (FOUND.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusFound();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusFound();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(FOUND, status));
                    }
                }
            }
        }

        public void hasStatusSeeOther() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (SEE_OTHER.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusSeeOther();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusSeeOther();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(SEE_OTHER, status));
                    }
                }
            }
        }

        public void hasStatusNotModified() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (NOT_MODIFIED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusNotModified();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusNotModified();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(NOT_MODIFIED, status));
                    }
                }
            }
        }

        public void hasStatusUseProxy() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (USE_PROXY.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusUseProxy();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusUseProxy();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(USE_PROXY, status));
                    }
                }
            }
        }

        public void hasStatusTemporaryRedirect() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (TEMPORARY_REDIRECT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusTemporaryRedirect();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusTemporaryRedirect();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(TEMPORARY_REDIRECT, status));
                    }
                }
            }
        }

        public void hasStatusBadRequest() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (BAD_REQUEST.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusBadRequest();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusBadRequest();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(BAD_REQUEST, status));
                    }
                }
            }
        }

        public void hasStatusUnauthorized() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (UNAUTHORIZED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusUnauthorized();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusUnauthorized();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(UNAUTHORIZED, status));
                    }
                }
            }
        }

        public void hasStatusPaymentRequired() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (PAYMENT_REQUIRED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusPaymentRequired();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusPaymentRequired();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(PAYMENT_REQUIRED, status));
                    }
                }
            }
        }

        public void hasStatusForbidden() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (FORBIDDEN.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusForbidden();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusForbidden();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(FORBIDDEN, status));
                    }
                }
            }
        }

        public void hasStatusNotFound() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (NOT_FOUND.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusNotFound();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusNotFound();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(NOT_FOUND, status));
                    }
                }
            }
        }

        public void hasStatusMethodNotAllowed() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (METHOD_NOT_ALLOWED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusMethodNotAllowed();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusMethodNotAllowed();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(METHOD_NOT_ALLOWED, status));
                    }
                }
            }
        }

        public void hasStatusNotAcceptable() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (NOT_ACCEPTABLE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusNotAcceptable();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusNotAcceptable();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(NOT_ACCEPTABLE, status));
                    }
                }
            }
        }

        public void hasStatusProxyAuthenticationRequired() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (PROXY_AUTHENTICATION_REQUIRED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusProxyAuthenticationRequired();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusProxyAuthenticationRequired();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(PROXY_AUTHENTICATION_REQUIRED, status));
                    }
                }
            }
        }

        public void hasStatusRequestTimeout() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (REQUEST_TIMEOUT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusRequestTimeout();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusRequestTimeout();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(REQUEST_TIMEOUT, status));
                    }
                }
            }
        }

        public void hasStatusConflict() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (CONFLICT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusConflict();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusConflict();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(CONFLICT, status));
                    }
                }
            }
        }

        public void hasStatusGone() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (GONE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusGone();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusGone();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(GONE, status));
                    }
                }
            }
        }

        public void hasStatusLengthRequired() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (LENGTH_REQUIRED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusLengthRequired();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusLengthRequired();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(LENGTH_REQUIRED, status));
                    }
                }
            }
        }

        public void hasStatusPreconditionFailed() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (PRECONDITION_FAILED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusPreconditionFailed();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusPreconditionFailed();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(PRECONDITION_FAILED, status));
                    }
                }
            }
        }

        public void hasStatusRequestEntityTooLarge() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (REQUEST_ENTITY_TOO_LARGE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusRequestEntityTooLarge();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusRequestEntityTooLarge();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(REQUEST_ENTITY_TOO_LARGE, status));
                    }
                }
            }
        }

        public void hasStatusRequestUriTooLong() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (REQUEST_URI_TOO_LONG.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusRequestUriTooLong();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusRequestUriTooLong();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(REQUEST_URI_TOO_LONG, status));
                    }
                }
            }
        }

        public void hasStatusUnsupportedMediaType() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (UNSUPPORTED_MEDIA_TYPE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusUnsupportedMediaType();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusUnsupportedMediaType();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(UNSUPPORTED_MEDIA_TYPE, status));
                    }
                }
            }
        }

        public void hasStatusRequestedRangeNotSatisfiable() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (REQUESTED_RANGE_NOT_SATISFIABLE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusRequestedRangeNotSatisfiable();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusRequestedRangeNotSatisfiable();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(REQUESTED_RANGE_NOT_SATISFIABLE, status));
                    }
                }
            }
        }

        public void hasStatusExpectationFailed() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (EXPECTATION_FAILED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusExpectationFailed();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusExpectationFailed();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(EXPECTATION_FAILED, status));
                    }
                }
            }
        }

        public void hasStatusInternalServerError() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (INTERNAL_SERVER_ERROR.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusInternalServerError();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusInternalServerError();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(INTERNAL_SERVER_ERROR, status));
                    }
                }
            }
        }

        public void hasStatusNotImplemented() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (NOT_IMPLEMENTED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusNotImplemented();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusNotImplemented();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(NOT_IMPLEMENTED, status));
                    }
                }
            }
        }

        public void hasStatusBadGateway() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (BAD_GATEWAY.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusBadGateway();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusBadGateway();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(BAD_GATEWAY, status));
                    }
                }
            }
        }

        public void hasStatusServiceUnavailable() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (SERVICE_UNAVAILABLE.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusServiceUnavailable();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusServiceUnavailable();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(SERVICE_UNAVAILABLE, status));
                    }
                }
            }
        }

        public void hasStatusGatewayTimeout() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (GATEWAY_TIMEOUT.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusGatewayTimeout();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusGatewayTimeout();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(GATEWAY_TIMEOUT, status));
                    }
                }
            }
        }

        public void hasStatusHttpVersionNotSupported() {
            for (Status status : Status.values()) {
                Response response = mock(Response.class);
                when(response.getStatus()).thenReturn(status.getStatusCode());

                if (HTTP_VERSION_NOT_SUPPORTED.equals(status)) {
                    ResponseAssert assertion = assertThat(response);
                    ResponseAssert returnValue = assertion.hasStatusHttpVersionNotSupported();
                    assertThat(returnValue).isSameAs(assertion);
                } else {
                    try {
                        assertThat(response).hasStatusHttpVersionNotSupported();
                        failBecauseExceptionWasNotThrown(AssertionError.class);
                    } catch (AssertionError e) {
                        assertThat(e).hasMessage(message(HTTP_VERSION_NOT_SUPPORTED, status));
                    }
                }
            }
        }

        // -------------------------------------------------------------------------------------------------------------

        public void hasStatusUnprocessableEntity() {
            final int unprocessableEntity = 422;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(unprocessableEntity);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusUnprocessableEntity();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusUnprocessableEntity();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(unprocessableEntity, OK));
            }
        }

        public void hasStatusMisdirectedRequest() {
            final int misdirectedRequest = 421;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(misdirectedRequest);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusMisdirectedRequest();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusMisdirectedRequest();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(misdirectedRequest, OK));
            }
        }

        public void hasStatusImATeapot() {
            final int imATeapot = 418;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(imATeapot);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusImATeapot();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusImATeapot();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(imATeapot, OK));
            }
        }

        public void hasStatusNetworkAuthenticationRequired() {
            final int networkAuthenticationRequired = 511;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(networkAuthenticationRequired);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusNetworkAuthenticationRequired();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusNetworkAuthenticationRequired();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(networkAuthenticationRequired, OK));
            }
        }

        public void hasStatusNotExtended() {
            final int notExtended = 510;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(notExtended);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusNotExtended();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusNotExtended();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(notExtended, OK));
            }
        }

        public void hasStatusLoopDetected() {
            final int loopDetected = 508;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(loopDetected);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusLoopDetected();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusLoopDetected();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(loopDetected, OK));
            }
        }

        public void hasStatusIMUsed() {
            final int iMUsed = 226;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(iMUsed);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusIMUsed();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusIMUsed();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(iMUsed, OK));
            }
        }

        public void hasStatusInsufficientStorage() {
            final int insufficientStorage = 507;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(insufficientStorage);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusInsufficientStorage();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusInsufficientStorage();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(insufficientStorage, OK));
            }
        }

        public void hasStatusVariantAlsoNegotiates() {
            final int variantAlsoNegotiates = 506;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(variantAlsoNegotiates);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusVariantAlsoNegotiates();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusVariantAlsoNegotiates();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(variantAlsoNegotiates, OK));
            }
        }

        public void hasStatusUnavailableForLegalReasons() {
            final int unavailableForLegalReasons = 451;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(unavailableForLegalReasons);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusUnavailableForLegalReasons();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusUnavailableForLegalReasons();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(unavailableForLegalReasons, OK));
            }
        }

        public void hasStatusPermanentRedirect() {
            final int permanentRedirect = 308;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(permanentRedirect);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusPermanentRedirect();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusPermanentRedirect();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(permanentRedirect, OK));
            }
        }

        public void hasStatusSwitchProxy() {
            final int switchProxy = 306;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(switchProxy);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusSwitchProxy();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusSwitchProxy();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(switchProxy, OK));
            }
        }

        public void hasStatusAlreadyReported() {
            final int alreadyReported = 208;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(alreadyReported);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusAlreadyReported();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusAlreadyReported();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(alreadyReported, OK));
            }
        }

        public void hasStatusMultiStatus() {
            final int multiStatus = 207;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(multiStatus);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusMultiStatus();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusMultiStatus();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(multiStatus, OK));
            }
        }

        public void hasStatusRequestHeaderFieldsTooLarge() {
            final int requestHeaderFieldsTooLarge = 431;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(requestHeaderFieldsTooLarge);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusRequestHeaderFieldsTooLarge();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusRequestHeaderFieldsTooLarge();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(requestHeaderFieldsTooLarge, OK));
            }
        }

        public void hasStatusTooManyRequests() {
            final int tooManyRequests = 429;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(tooManyRequests);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusTooManyRequests();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusTooManyRequests();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(tooManyRequests, OK));
            }
        }

        public void hasStatusPreconditionRequired() {
            final int preconditionRequired = 428;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(preconditionRequired);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusPreconditionRequired();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusPreconditionRequired();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(preconditionRequired, OK));
            }
        }

        public void hasStatusUpgradeRequired() {
            final int upgradeRequired = 426;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(upgradeRequired);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusUpgradeRequired();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusUpgradeRequired();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(upgradeRequired, OK));
            }
        }

        public void hasStatusFailedDependency() {
            final int failedDependency = 424;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(failedDependency);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusFailedDependency();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusFailedDependency();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(failedDependency, OK));
            }
        }

        public void hasStatusLocked() {
            final int locked = 423;
            Response response = mock(Response.class);

            when(response.getStatus()).thenReturn(locked);
            ResponseAssert assertion = assertThat(response);
            ResponseAssert returnValue = assertion.hasStatusLocked();
            assertThat(returnValue).isSameAs(assertion);

            when(response.getStatus()).thenReturn(OK.getStatusCode());
            try {
                assertThat(response).hasStatusLocked();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(message(locked, OK));
            }
        }

        // -------------------------------------------------------------------------------------------------------------

        private String message(Status expectedStatus, Status actualStatus) {
            return message(expectedStatus.getStatusCode(), actualStatus.getStatusCode());
        }

        private String message(int expectedStatus, Status actualStatus) {
            return message(expectedStatus, actualStatus.getStatusCode());
        }

        private String message(int expectedStatus, int actualStatus) {
            return format("Expected status code to be <%d> but was <%d>", expectedStatus, actualStatus);
        }
    }

}
