package name.didier.david.test4j.assertions;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class EqualsVerifierTest {

    private static final String CODE = "CODE";
    private static final String ANOTHER_CODE = "another code";

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_a_correct_implementation {

        private CorrectImpl object;

        @BeforeMethod
        public void create_object() {
            object = new CorrectImpl(CODE);
        }

        public void should_obviously_pass() {
            new EqualsVerifier(object).verify();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_a_correct_implementation_tested_against_equal_objects {

        private EqualsVerifier verifier;
        private CorrectImpl object;

        @BeforeMethod
        public void create_object() {
            object = new CorrectImpl(CODE);
            verifier = new EqualsVerifier(object);
        }

        public void should_pass_with_itself() {
            verifier.mustBeEqualsTo(object).verify();
        }

        public void should_pass_with_an_object_with_the_same_code() {
            verifier.mustBeEqualsTo(new CorrectImpl(CODE)).verify();
        }

        @Test(expectedExceptions = IllegalArgumentException.class)
        public void should_fail_with_a_null_object() {
            verifier.mustBeEqualsTo(null).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_with_an_object_of_a_different_class() {
            verifier.mustBeEqualsTo(new Object()).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_with_an_object_with_a_different_code() {
            verifier.mustBeEqualsTo(new CorrectImpl(ANOTHER_CODE)).verify();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_a_correct_implementation_tested_against_unequal_objects {

        private EqualsVerifier verifier;
        private CorrectImpl object;

        @BeforeMethod
        public void create_object() {
            object = new CorrectImpl(CODE);
            verifier = new EqualsVerifier(object);
        }

        public void should_pass_with_an_object_with_a_different_code() {
            verifier.mustNotBeEqualsTo(new CorrectImpl(ANOTHER_CODE)).verify();
        }

        @Test(expectedExceptions = IllegalArgumentException.class)
        public void should_fail_with_a_null_object() {
            verifier.mustNotBeEqualsTo(null).verify();
        }

        public void should_pass_with_an_object_of_a_different_class() {
            verifier.mustNotBeEqualsTo(new Object()).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_with_an_object_with_the_same_code() {
            verifier.mustNotBeEqualsTo(new CorrectImpl(CODE)).verify();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_bad_implementations {

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_null_is_equals() {
            new EqualsVerifier(new EqualsIfNullImpl(CODE)).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_same_is_unequals() {
            new EqualsVerifier(new UnequalsIfSameImpl(CODE)).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_different_classes_are_equals() {
            new EqualsVerifier(new EqualsIfDifferentClassesImpl(CODE)).mustNotBeEqualsTo(new Object()).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_hashcode_is_not_the_same_between_invocations() {
            new EqualsVerifier(new RandomHashCodeImpl(CODE)).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_hashcode_is_not_the_same_between_equals_instances() {
            new EqualsVerifier(new UnequalInstanceHashCodeImpl(CODE))
                    .mustBeEqualsTo(new UnequalInstanceHashCodeImpl(CODE)).verify();
        }

        @Test(expectedExceptions = AssertionError.class)
        public void should_fail_if_hashcode_is_the_same_between_unequals_instances() {
            new EqualsVerifier(new ConstantHashCodeImpl(CODE)).mustNotBeEqualsTo(new ConstantHashCodeImpl(ANOTHER_CODE))
                    .verify();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private abstract static class AbstractObject {

        protected final String code;

        protected AbstractObject(final String code) {
            super();
            this.code = code;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private abstract static class AbstractEqualsObject
            extends AbstractObject {

        protected AbstractEqualsObject(final String code) {
            super(code);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(code).toHashCode();
        }

        @Override
        public boolean equals(final Object object) {
            return false;
        }

        protected boolean equalsWithCode(final Object object) {
            // CSOFF: IllegalTypeCheck 1
            AbstractEqualsObject rhs = (AbstractEqualsObject) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(code, rhs.code);
            return builder.isEquals();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class CorrectImpl
            extends AbstractEqualsObject {

        protected CorrectImpl(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            if (object == null) {
                return false;
            }
            if (object == this) {
                return true;
            }
            if (object.getClass() != getClass()) {
                return false;
            }

            return equalsWithCode(object);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class EqualsIfNullImpl
            extends AbstractEqualsObject {

        protected EqualsIfNullImpl(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            if (object == null) {
                // incorrect
                return true;
            }
            if (object == this) {
                return true;
            }
            if (object.getClass() != getClass()) {
                return false;
            }

            return equalsWithCode(object);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class UnequalsIfSameImpl
            extends AbstractEqualsObject {

        protected UnequalsIfSameImpl(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            if (object == null) {
                return false;
            }
            if (object == this) {
                // incorrect
                return false;
            }
            if (object.getClass() != getClass()) {
                return false;
            }

            CorrectImpl rhs = (CorrectImpl) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(code, rhs.code);

            return equalsWithCode(object);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class EqualsIfDifferentClassesImpl
            extends AbstractEqualsObject {

        protected EqualsIfDifferentClassesImpl(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            if (object == null) {
                return false;
            }
            if (object == this) {
                return true;
            }
            if (object.getClass() != getClass()) {
                // incorrect
                return true;
            }

            return equalsWithCode(object);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private abstract static class AbstractHashCodeObject
            extends AbstractObject {

        protected AbstractHashCodeObject(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            if (object == null) {
                return false;
            }
            if (object == this) {
                return true;
            }
            if (object.getClass() != getClass()) {
                return false;
            }

            // CSOFF: IllegalTypeCheck 1
            AbstractHashCodeObject rhs = (AbstractHashCodeObject) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(code, rhs.code);
            return builder.isEquals();
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class RandomHashCodeImpl
            extends AbstractHashCodeObject {

        protected RandomHashCodeImpl(final String code) {
            super(code);
        }

        @Override
        public int hashCode() {
            // CSOFF: MagicNumberCheck +1
            return RandomStringUtils.random(100).hashCode();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class UnequalInstanceHashCodeImpl
            extends AbstractHashCodeObject {

        private final int hashCode;

        protected UnequalInstanceHashCodeImpl(final String code) {
            super(code);
            // CSOFF: MagicNumberCheck +1
            hashCode = RandomStringUtils.random(100).hashCode();
        }

        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class ConstantHashCodeImpl
            extends AbstractHashCodeObject {

        protected ConstantHashCodeImpl(final String code) {
            super(code);
        }

        @Override
        public boolean equals(final Object object) {
            return false;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }
}
