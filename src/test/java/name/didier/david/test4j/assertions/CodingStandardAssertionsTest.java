package name.didier.david.test4j.assertions;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

import name.didier.david.test4j.coverage.CoverageUtils;

@Test
public class CodingStandardAssertionsTest {

    @Test
    public static class describe_CodingStandardAssertions {

        public void should_be_covered() {
            CoverageUtils.coverConstructor(CodingStandardAssertions.class);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_assertThat_Class {

        public void should_return_a_new_CodingStandardClassAssert_given_a_class() {
            assertThat(CodingStandardAssertions.assertThat(String.class)).isInstanceOf(CodingStandardClassAssert.class);
        }
    }
}
