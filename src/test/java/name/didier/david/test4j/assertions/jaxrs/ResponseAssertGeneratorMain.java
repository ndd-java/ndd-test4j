package name.didier.david.test4j.assertions.jaxrs;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_UNDERSCORE;

import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Generate the assertions for {@link Response}.
 *
 * @author ddidier
 */
public final class ResponseAssertGeneratorMain {

    /** Utility class. */
    private ResponseAssertGeneratorMain() {
        super();
    }

    public static void main(String[] args) {
        // CSOFF: MagicNumberCheck
        final Map<Integer, String> otherCodes = new Hashtable<>();
        otherCodes.put(207, "MultiStatus");
        otherCodes.put(208, "AlreadyReported");
        otherCodes.put(226, "IMUsed");
        otherCodes.put(306, "SwitchProxy");
        otherCodes.put(308, "PermanentRedirect");
        otherCodes.put(418, "ImATeapot");
        otherCodes.put(421, "MisdirectedRequest");
        otherCodes.put(422, "UnprocessableEntity");
        otherCodes.put(423, "Locked");
        otherCodes.put(424, "FailedDependency");
        otherCodes.put(426, "UpgradeRequired");
        otherCodes.put(428, "PreconditionRequired");
        otherCodes.put(429, "TooManyRequests");
        otherCodes.put(431, "RequestHeaderFieldsTooLarge");
        otherCodes.put(451, "UnavailableForLegalReasons");
        otherCodes.put(506, "VariantAlsoNegotiates");
        otherCodes.put(507, "InsufficientStorage");
        otherCodes.put(508, "LoopDetected");
        otherCodes.put(510, "NotExtended");
        otherCodes.put(511, "NetworkAuthenticationRequired");
        // CSON: MagicNumberCheck

        for (Status status : Response.Status.values()) {
            toAssert(status);
        }

        for (Entry<Integer, String> entry : otherCodes.entrySet()) {
            toAssert(entry.getKey(), entry.getValue());
        }

        toStdOut("===================================================================================================");

        for (Status status : Response.Status.values()) {
            toAssertTest(status);
        }

        for (Entry<Integer, String> entry : otherCodes.entrySet()) {
            toAssertTest(entry.getKey(), entry.getValue());
        }
    }

    public static void toAssert(Status status) {
        String nameSnakeCase = status.name();
        // CSOFF: VariableDeclarationUsageDistanceCheck +1
        String nameCamelCase = UPPER_UNDERSCORE.to(UPPER_CAMEL, nameSnakeCase);
        int statusCode = status.getStatusCode();

        toStdOut("/**");
        toStdOut(" * Verifies that the actual status is %s (%d).", nameSnakeCase, statusCode);
        toStdOut(" *");
        toStdOut(" * @return this assertion.");
        toStdOut(" * @throws AssertionError if the actual status is not %s.", nameSnakeCase);
        toStdOut(" */");
        toStdOut(" public ResponseAssert hasStatus%s() {", nameCamelCase);
        toStdOut("     return assertStatus(%s);", nameSnakeCase);
        toStdOut(" }");
        toStdOut("");
    }

    public static void toAssert(Integer statusCode, String nameCamelCase) {
        // CSOFF: VariableDeclarationUsageDistanceCheck +1
        String nameSnakeCase = UPPER_CAMEL.to(UPPER_UNDERSCORE, nameCamelCase);

        toStdOut("/**");
        toStdOut(" * Verifies that the actual status is %s (%d).", nameSnakeCase, statusCode);
        toStdOut(" *");
        toStdOut(" * @return this assertion.");
        toStdOut(" * @throws AssertionError if the actual status is not %s.", nameSnakeCase);
        toStdOut(" */");
        toStdOut(" public ResponseAssert hasStatus%s() {", nameCamelCase);
        toStdOut("     return assertStatus(%d);", statusCode);
        toStdOut(" }");
        toStdOut("");
    }

    public static void toAssertTest(Status status) {
        String nameSnakeCase = status.name();
        String nameCamelCase = UPPER_UNDERSCORE.to(UPPER_CAMEL, nameSnakeCase);

        toStdOut("public void hasStatus%s() {", nameCamelCase);
        toStdOut("    for (Status status : Status.values()) {");
        toStdOut("        Response response = mock(Response.class);");
        toStdOut("        when(response.getStatus()).thenReturn(status.getStatusCode());");
        toStdOut("");
        toStdOut("        if (%S.equals(status)) {", nameSnakeCase);
        toStdOut("            ResponseAssert assertion = assertThat(response);");
        toStdOut("            ResponseAssert returnValue = assertion.hasStatus%s();", nameCamelCase);
        toStdOut("            assertThat(returnValue).isSameAs(assertion);");
        toStdOut("        } else {");
        toStdOut("            try {");
        toStdOut("                assertThat(response).hasStatus%s();", nameCamelCase);
        toStdOut("                failBecauseExceptionWasNotThrown(AssertionError.class);");
        toStdOut("            } catch (AssertionError e) {");
        toStdOut("                assertThat(e).hasMessage(message(%s, status));", nameSnakeCase);
        toStdOut("           }");
        toStdOut("        }");
        toStdOut("    }");
        toStdOut("}");
    }

    public static void toAssertTest(Integer statusCode, String nameCamelCase) {
        String lowerNameCamelCase = UPPER_CAMEL.to(LOWER_CAMEL, nameCamelCase);

        toStdOut("public void hasStatus%s() {", nameCamelCase);
        toStdOut("    final int %s = %s;", lowerNameCamelCase, statusCode);
        toStdOut("    Response response = mock(Response.class);");
        toStdOut("");
        toStdOut("    when(response.getStatus()).thenReturn(%s);", lowerNameCamelCase);
        toStdOut("    ResponseAssert assertion = assertThat(response);");
        toStdOut("    ResponseAssert returnValue = assertion.hasStatus%s();", nameCamelCase);
        toStdOut("    assertThat(returnValue).isSameAs(assertion);");
        toStdOut("");
        toStdOut("    when(response.getStatus()).thenReturn(OK.getStatusCode());");
        toStdOut("    try {");
        toStdOut("        assertThat(response).hasStatus%s();", nameCamelCase);
        toStdOut("        failBecauseExceptionWasNotThrown(AssertionError.class);");
        toStdOut("    } catch (AssertionError e) {");
        toStdOut("        assertThat(e).hasMessage(message(%s, OK));", lowerNameCamelCase);
        toStdOut("    }");
        toStdOut("}");
    }

    private static void toStdOut(String format, Object... args) {
        // CSOFF: RegexpCheck +1
        System.out.printf(format + "\n", args);
    }
}
