package name.didier.david.test4j.dbunit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.VerifyTableDefinition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.zapodot.junit.db.EmbeddedDatabaseRule;

import junit.framework.ComparisonFailure;
import name.didier.david.test4j.junit.TestResourceRule;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DbUnitTestSupportTest.describe_misc.class,
        DbUnitTestSupportTest.describe_test.class,
        DbUnitTestSupportTest.describe_testReadOnly.class })
public class DbUnitTestSupportTest {

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_misc {

        @Test
        @SuppressWarnings("unused")
        public void constructor_rejects_a_null_DataSource() {
            try {
                new DbUnitTestSupport(null);
                failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
            } catch (IllegalArgumentException e) {
                assertThat(e).hasMessageContaining("dataSource");
            }
        }

        @Test
        public void getDataSource_returns_the_underlying_DataSource() {
            DataSource dataSource = mock(DataSource.class);
            DbUnitTestSupport dbUnitSupport = new DbUnitTestSupport(dataSource);
            assertThat(dbUnitSupport.getDataSource()).isSameAs(dataSource);
        }

        @Test
        public void verifyTables_returns_an_array_of_VerifyTableDefinition_with_all_columns() {
            assertThat(DbUnitTestSupport.verifyTables("table1"))
                    .extracting("tableName", "columnExclusionFilters", "columnInclusionFilters")
                    .containsExactly(tuple("table1", new String[0], null));

            assertThat(DbUnitTestSupport.verifyTables("table1", "table2"))
                    .extracting("tableName", "columnExclusionFilters", "columnInclusionFilters")
                    .containsExactly(tuple("table1", new String[0], null), tuple("table2", new String[0], null));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_test
            extends AbstractTestDescription {

        @Test
        public void with_a_successful_expected_comparison() {
            dbUnitSupport.test(seedDataPaths, expectedDataPaths, verifiedTables, () -> {
                insertOneAuthor();
            });
        }

        @Test
        public void with_a_successful_expected_comparison_with_a_returned_value() {
            String result = (String) dbUnitSupport.test(seedDataPaths, expectedDataPaths, verifiedTables, () -> {
                insertOneAuthor();
                return "some value";
            });

            assertThat(result).isEqualTo("some value");
        }

        @Test
        public void with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.test(seedDataPaths, expectedDataPaths, verifiedTables, () -> {
                    // empty
                });
                failBecauseExceptionWasNotThrown(ComparisonFailure.class);
            } catch (ComparisonFailure e) {
                assertThat(e).hasMessageContaining("row count (table=author) expected:<[2]> but was:<[1]>");
            }
        }

        @Test
        public void with_an_error_raised_in_the_lambda_with_a_successful_expected_comparison() {
            try {
                dbUnitSupport.test(seedDataPaths, expectedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw RUNTIME_EXCEPTION;
                });
                failBecauseExceptionWasNotThrown(RuntimeException.class);
            } catch (RuntimeException e) {
                assertThat(e).hasMessage("Error while testing database state");
                assertThat(e.getCause()).isInstanceOf(RuntimeException.class);
                assertThat(e.getCause()).hasMessage(RUNTIME_EXCEPTION.getMessage());
            }
        }

        @Test
        public void with_an_AssertionError_raised_in_the_lambda_with_a_successful_expected_comparison() {
            try {
                dbUnitSupport.test(seedDataPaths, expectedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw ASSERTION_ERROR;
                });
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(ASSERTION_ERROR.getMessage());
            }
        }

        @Test
        public void with_an_error_raised_in_the_lambda_with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.test(seedDataPaths, seedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw RUNTIME_EXCEPTION;
                });
                failBecauseExceptionWasNotThrown(RuntimeException.class);
            } catch (RuntimeException e) {
                assertThat(e).hasMessage("Error while testing database state");
                assertThat(e.getCause()).isInstanceOf(RuntimeException.class);
                assertThat(e.getCause()).hasMessage(RUNTIME_EXCEPTION.getMessage());
            }
        }

        @Test
        public void with_an_AssertionError_raised_in_the_lambda_with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.test(seedDataPaths, seedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw ASSERTION_ERROR;
                });
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(ASSERTION_ERROR.getMessage());
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_testReadOnly
            extends AbstractTestDescription {

        @Test
        public void with_a_successful_expected_comparison() {
            dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                // empty
            });
        }

        @Test
        public void with_a_successful_expected_comparison_with_a_returned_value() {
            String result = (String) dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                return "some value";
            });
            assertThat(result).isEqualTo("some value");
        }

        @Test
        public void with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                });
                failBecauseExceptionWasNotThrown(ComparisonFailure.class);
            } catch (ComparisonFailure e) {
                assertThat(e).hasMessageContaining("row count (table=author) expected:<[1]> but was:<[2]>");
            }
        }

        @Test
        public void with_an_error_raised_in_the_lambda_with_a_successful_expected_comparison() {
            try {
                dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                    throw RUNTIME_EXCEPTION;
                });
                failBecauseExceptionWasNotThrown(RuntimeException.class);
            } catch (RuntimeException e) {
                assertThat(e).hasMessage("Error while testing database state");
                assertThat(e.getCause()).isInstanceOf(RuntimeException.class);
                assertThat(e.getCause()).hasMessage(RUNTIME_EXCEPTION.getMessage());
            }
        }

        @Test
        public void with_an_AssertionError_raised_in_the_lambda_with_a_successful_expected_comparison() {
            try {
                dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                    throw ASSERTION_ERROR;
                });
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(ASSERTION_ERROR.getMessage());
            }
        }

        @Test
        public void with_an_error_raised_in_the_lambda_with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw RUNTIME_EXCEPTION;
                });
                failBecauseExceptionWasNotThrown(RuntimeException.class);
            } catch (RuntimeException e) {
                assertThat(e).hasMessage("Error while testing database state");
                assertThat(e.getCause()).isInstanceOf(RuntimeException.class);
                assertThat(e.getCause()).hasMessage(RUNTIME_EXCEPTION.getMessage());
            }
        }

        @Test
        public void with_an_AssertionError_raised_in_the_lambda_with_a_failed_expected_comparison() {
            try {
                dbUnitSupport.testReadOnly(seedDataPaths, verifiedTables, () -> {
                    insertOneAuthor();
                    throw ASSERTION_ERROR;
                });
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessage(ASSERTION_ERROR.getMessage());
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract static class AbstractTestDescription {

        protected static final AssertionError ASSERTION_ERROR = new AssertionError("an AssertionError");
        protected static final RuntimeException RUNTIME_EXCEPTION = new RuntimeException("not an AssertionError");

        @Rule
        public EmbeddedDatabaseRule h2Rule = EmbeddedDatabaseRule.builder()
                .withInitialSql("CREATE TABLE author(id INTEGER PRIMARY KEY, name VARCHAR(512));")
                .build();
        @Rule
        public TestResourceRule testResources = new TestResourceRule();

        protected DataSource dataSource;
        protected DbUnitTestSupport dbUnitSupport;

        protected String[] seedDataPaths;
        protected String[] expectedDataPaths;
        protected VerifyTableDefinition[] verifiedTables;

        @Before
        public void setUp()
                throws Exception {
            dataSource = mock(DataSource.class);
            when(dataSource.getConnection()).thenReturn(h2Rule.getConnection());

            dbUnitSupport = new DbUnitTestSupport(dataSource);

            seedDataPaths = new String[] { testResources.toPath("/{p}/DbUnitTestSupportTest-data/seeds.xml") };
            expectedDataPaths = new String[] { testResources.toPath("/{p}/DbUnitTestSupportTest-data/expected.xml") };
            verifiedTables = new VerifyTableDefinition[] { new VerifyTableDefinition("author", new String[0]) };
        }

        protected void insertOneAuthor()
                throws SQLException {
            try (
                    Connection connection = h2Rule.getConnection();
                    PreparedStatement statement = connection.prepareStatement("INSERT INTO author (id) VALUES (?)")) {

                statement.setInt(1, 2);
                statement.executeUpdate();
            }
        }
    }
}
