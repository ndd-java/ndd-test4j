package name.didier.david.test4j.dbunit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.google.common.io.Files;

import name.didier.david.test4j.testng.TestNgResourceListener;
import name.didier.david.test4j.testng.TestNgResources;

@Test
@Listeners(TestNgResourceListener.class)
public class DatabaseSchemaExporterTest {

    private static final String SQL = "CREATE TABLE author(id INTEGER PRIMARY KEY, name VARCHAR(512))\\;"
            + "CREATE TABLE post(id INTEGER PRIMARY KEY, title VARCHAR(512), author_id INTEGER,"
            + "    FOREIGN KEY (author_id) REFERENCES author(id))\\;";
    private static final String URL = "jdbc:h2:mem:test";

    private static Connection connection;

    private TestNgResources testResources;
    private DatabaseSchemaExporter exporter;

    @BeforeClass
    public static void set_up_connection()
            throws Exception {
        connection = DriverManager.getConnection(URL + ";INIT=" + SQL);
    }

    @AfterClass
    public static void tear_down_connection()
            throws Exception {
        connection.close();
    }

    @Before
    public void create_exporter()
            throws Exception {
        exporter = DatabaseSchemaExporter.from(connection);
    }

    public void from_rejects_a_null_Connection() {
        try {
            DatabaseSchemaExporter.from((Connection) null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("connection");
        }
    }

    public void from_rejects_a_null_DataSource() {
        try {
            DatabaseSchemaExporter.from((DataSource) null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("dataSource");
        }
    }

    public void from_rejects_a_null_URL()
            throws Exception {
        try {
            DatabaseSchemaExporter.from(null, "user", "password");
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("url");
        }
    }

    public void from_builds_an_exporter_from_an_URL()
            throws Exception {
        assertThat(DatabaseSchemaExporter.from(URL, "", "")).isNotNull();
    }

    public void exportDtd_exports_the_schema_of_a_valid_database() {
        String actualSchema = exporter.exportDtd();
        String expectedSchema = testResources.asContent("/{p}/{c}-data/schema.dtd");
        assertThat(actualSchema).isEqualTo(expectedSchema);
    }

    public void exportDtdTo_rejects_a_null_file() {
        try {
            exporter.exportDtdTo(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("destinationFile");
        }
    }

    public void exportDtdTo_exports_the_schema_of_a_valid_database_to_a_file()
            throws Exception {
        File directory = Files.createTempDir();
        try {
            File actualSchema = new File(directory, "schema.dtd");
            exporter.exportDtdTo(actualSchema);
            String expectedSchema = testResources.asContent("/{p}/{c}-data/schema.dtd");
            assertThat(actualSchema).hasContent(expectedSchema);
        } finally {
            if (directory.exists()) {
                FileUtils.deleteDirectory(directory);
            }
        }
    }
}
