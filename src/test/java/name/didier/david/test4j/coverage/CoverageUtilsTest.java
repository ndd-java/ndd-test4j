package name.didier.david.test4j.coverage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.lang.reflect.InvocationTargetException;

import org.testng.annotations.Test;

import name.didier.david.test4j.assertions.CodingStandardAssertions;

@Test
public class CoverageUtilsTest {

    @Test
    public static class describe_CoverageUtils {

        public void should_follow_coding_standards() {
            CodingStandardAssertions.assertThat(CoverageUtils.class).isUtility();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_cover_Enum {

        public void should_be_covered() {
            CoverageUtils.cover(MyEnum.class);
        }

        // ------------------------------

        private enum MyEnum {
            A,
            B,
            C
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_coverConstructor_Class {

        public void should_be_covered() {
            CoverageUtils.coverConstructor(MyUtilityClass.class);
        }

        public void should_be_covered_when_failing() {
            try {
                CoverageUtils.coverConstructor(MyUtilityClassWithException.class);
                failBecauseExceptionWasNotThrown(RuntimeException.class);
            } catch (RuntimeException e) {
                assertThat(e).hasCauseExactlyInstanceOf(InvocationTargetException.class);
                assertThat(e.getCause()).hasCauseExactlyInstanceOf(IllegalArgumentException.class);
            }
        }

        // ------------------------------

        private static final class MyUtilityClass {
            private MyUtilityClass() {
                super();
            }
        }

        private static final class MyUtilityClassWithException {
            private MyUtilityClassWithException() {
                throw new IllegalArgumentException();
            }
        }
    }
}
