package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

@Test
public class NoExitExceptionTest {

    public void test() {
        String message = "some message";
        int status = 1;
        NoExitException e = new NoExitException(message, status);
        assertThat(e).hasMessage(message);
        assertThat(e.getStatus()).isEqualTo(1);
    }
}
