package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.test4j.testng.TestNgDataProviders.NEGATIVE_NUMBERS;

import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgDataProviders;

@Test
public class StdCaptorTest {

    private StdCaptor captor;

    @BeforeMethod
    public void create_captor() {
        captor = new StdCaptor();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NEGATIVE_NUMBERS)
    @SuppressWarnings("unused")
    public void constructor_should_reject_any_negative_buffer_size(final int bufferSize) {
        try {
            new StdCaptor(bufferSize);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("bufferSize");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_capture_stdout() {
        String message = "message to STDOUT 1";
        try {
            captor.startStdOutCapture();
            // CSOFF: RegexpCheck +1
            System.out.println(message);
        } catch (Exception e) {
            fail(e);
        } finally {
            String capture = captor.stopStdOutCapture();
            assertThat(capture).isEqualTo(message + '\n');
        }
    }

    public void should_capture_stdout_then_print() {
        String message = "message to STDOUT 2";
        try {
            captor.startStdOutCapture();
            StdCaptor captor2 = new StdCaptor();

            try {
                captor2.startStdOutCapture();
                // CSOFF: RegexpCheck +1
                System.out.println(message);
            } catch (Exception e) {
                fail(e);
            } finally {
                captor2.stopStdOutCapture(true);
            }

        } catch (Exception e) {
            fail(e);
        } finally {
            String capture = captor.stopStdOutCapture();
            assertThat(capture).isEqualTo(message + '\n');
        }
    }

    public void should_fail_while_starting_a_captured_stdout() {
        captor.startStdOutCapture();
        try {
            captor.startStdOutCapture();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Already capturing STDOUT");
        } finally {
            captor.stopStdOutCapture();
        }
    }

    public void should_fail_while_stopping_a_not_captured_stdout() {
        try {
            captor.stopStdOutCapture();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Not capturing STDOUT");
        }
    }

    public void should_test_the_capture_state_of_stdout() {
        try {
            assertThat(captor.isCapturingStdOut()).isFalse();
            captor.startStdOutCapture();
            assertThat(captor.isCapturingStdOut()).isTrue();
        } catch (Exception e) {
            fail(e);
        } finally {
            captor.stopStdOutCapture();
            assertThat(captor.isCapturingStdOut()).isFalse();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_capture_stderr() {
        String message = "message to STDERR 1";
        try {
            captor.startStdErrCapture();
            // CSOFF: RegexpCheck +1
            System.err.println(message);
        } catch (Exception e) {
            fail(e);
        } finally {
            String capture = captor.stopStdErrCapture();
            assertThat(capture).isEqualTo(message + '\n');
        }
    }

    public void should_capture_stderr_then_print() {
        String message = "message to STDERR 2";
        try {
            captor.startStdErrCapture();
            StdCaptor captor2 = new StdCaptor();

            try {
                captor2.startStdErrCapture();
                // CSOFF: RegexpCheck +1
                System.err.println(message);
            } catch (Exception e) {
                fail(e);
            } finally {
                captor2.stopStdErrCapture(true);
            }

        } catch (Exception e) {
            fail(e);
        } finally {
            String capture = captor.stopStdErrCapture();
            assertThat(capture).isEqualTo(message + '\n');
        }
    }

    public void should_fail_while_starting_a_captured_stderr() {
        captor.startStdErrCapture();
        try {
            captor.startStdErrCapture();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Already capturing STDERR");
        } finally {
            captor.stopStdErrCapture();
        }
    }

    public void should_fail_while_stopping_a_not_captured_stderr() {
        try {
            captor.stopStdErrCapture();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Not capturing STDERR");
        }
    }

    public void should_test_the_capture_state_of_stderr() {
        try {
            assertThat(captor.isCapturingStdErr()).isFalse();
            captor.startStdErrCapture();
            assertThat(captor.isCapturingStdErr()).isTrue();
        } catch (Exception e) {
            fail(e);
        } finally {
            captor.stopStdErrCapture();
            assertThat(captor.isCapturingStdErr()).isFalse();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static void fail(final Exception e) {
        Assertions.fail("Unepected error", e);
    }
}
