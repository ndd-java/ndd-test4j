package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class ExitCaptorTest {

    private ExitCaptor captor;

    @BeforeMethod
    public void create_captor() {
        captor = new ExitCaptor();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_capture_exit_call() {
        try {
            captor.startCapturing();
            System.exit(1);
        } catch (NoExitException e) {
            assertThat(e.getStatus()).isEqualTo(1);
        } catch (Exception e) {
            fail(e);
        } finally {
            captor.stopCapturing();
        }
    }

    public void should_fail_while_capturing_captured_exit_calls() {
        captor.startCapturing();
        try {
            captor.startCapturing();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Already capturing exit calls");
        } finally {
            captor.stopCapturing();
        }
    }

    public void should_fail_while_stopping_not_captured_exit_calls() {
        try {
            captor.stopCapturing();
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Not capturing exit calls");
        }
    }

    public void should_test_the_capturing_state_of_exit_calls() {
        try {
            assertThat(captor.isCapturing()).isFalse();
            captor.startCapturing();
            assertThat(captor.isCapturing()).isTrue();
        } catch (Exception e) {
            fail(e);
        } finally {
            captor.stopCapturing();
            assertThat(captor.isCapturing()).isFalse();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static void fail(final Exception e) {
        Assertions.fail("Unepected error", e);
    }
}
