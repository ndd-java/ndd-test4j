package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class NoExitSecurityManagerTest {

    private NoExitSecurityManager manager;

    @BeforeMethod
    public void create_manager() {
        manager = new NoExitSecurityManager();
    }

    public void checkExit() {
        try {
            manager.checkExit(1);
            failBecauseExceptionWasNotThrown(NoExitException.class);
        } catch (NoExitException e) {
            assertThat(e).hasMessage("Intercepted exit with status 1");
            assertThat(e.getStatus()).isEqualTo(1);
        }
    }

    public void checkPermissionPermission() {
        manager.checkPermission(null);
        // no exception raised
    }

    public void checkPermissionPermissionObject() {
        manager.checkPermission(null, null);
        // no exception raised
    }
}
