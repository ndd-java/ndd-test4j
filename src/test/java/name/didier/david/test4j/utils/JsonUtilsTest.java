package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.utils.JsonUtils.json;
import static name.didier.david.test4j.utils.JsonUtils.normalize;
import static name.didier.david.test4j.utils.JsonUtils.normalizeJson;
import static name.didier.david.test4j.utils.JsonUtils.prettyPrint;
import static name.didier.david.test4j.utils.JsonUtils.prettyPrintJson;

import org.testng.annotations.Test;

@Test
public class JsonUtilsTest {

    @Test
    public static class describe_json {

        public void replace_single_quotes_with_double_quotes() {
            assertThat(json("{'id': 123, 'name': 'a cool name'}"))
                    .isEqualTo("{\"id\": 123, \"name\": \"a cool name\"}");
        }

        public void replace_custom_quotes_with_double_quotes() {
            assertThat(json('@', "{@id@: 123, @name@: @a cool name@}"))
                    .isEqualTo("{\"id\": 123, \"name\": \"a cool name\"}");
        }

        public void replace_single_quotes_with_double_quotes_and_join_lines() {
            assertThat(json(
                    "{",
                    "  'id': 123,",
                    "  'name': 'a cool name'",
                    "}"))
                            .isEqualTo("{\n  \"id\": 123,\n  \"name\": \"a cool name\"\n}");
        }

        public void replace_custom_quotes_with_double_quotes_and_join_lines() {
            assertThat(json('@',
                    "{",
                    "  @id@: 123,",
                    "  @name@: @a cool name@",
                    "}"))
                            .isEqualTo("{\n  \"id\": 123,\n  \"name\": \"a cool name\"\n}");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_normalize {

        public void normalize_json() {
            assertThat(normalize(json("{'id': 123, 'name': 'a cool name'}")))
                    .isEqualTo("{\"id\":123,\"name\":\"a cool name\"}");
        }

        public void replace_single_quotes_with_double_quotes_then_normalize_json() {
            assertThat(normalizeJson("{'id': 123, 'name': 'a cool name'}"))
                    .isEqualTo("{\"id\":123,\"name\":\"a cool name\"}");
        }

        public void replace_single_quotes_with_double_quotes_and_join_lines_then_normalize_json() {
            assertThat(normalizeJson(
                    "{",
                    "  'id': 123,",
                    "  'name': 'a cool name'",
                    "}"))
                            .isEqualTo("{\"id\":123,\"name\":\"a cool name\"}");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_prettyPrint {

        public void pretty_print_json() {
            assertThat(prettyPrint(json("{'id': 123, 'name': 'a cool name'}")))
                    .isEqualTo("{\n  \"id\" : 123,\n  \"name\" : \"a cool name\"\n}");
        }

        public void replace_single_quotes_with_double_quotes_then_pretty_print_json() {
            assertThat(prettyPrintJson("{'id': 123, 'name': 'a cool name'}"))
                    .isEqualTo("{\n  \"id\" : 123,\n  \"name\" : \"a cool name\"\n}");
        }

        public void replace_single_quotes_with_double_quotes_and_join_lines_then_pretty_print_json() {
            assertThat(prettyPrintJson(
                    "{",
                    "  'id': 123,",
                    "  'name': 'a cool name'",
                    "}"))
                            .isEqualTo("{\n  \"id\" : 123,\n  \"name\" : \"a cool name\"\n}");
        }
    }

}
