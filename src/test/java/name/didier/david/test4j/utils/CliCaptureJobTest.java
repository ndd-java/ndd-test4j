package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.testng.annotations.Test;

@Test
public class CliCaptureJobTest {

    public void should_capture_exit_call_and_streams() {
        final String messageStdOut = "message to STDOUT 1";
        final String messageStdErr = "message to STDERR 1";

        CliCaptureJob captureJob = new CliCaptureJob() {
            @Override
            protected void execute() {
                // CSOFF: Regexp +2
                System.out.println(messageStdOut);
                System.err.println(messageStdErr);
                System.exit(-1);
            }

            @Override
            protected void checkStatus(final int status) {
                assertThat(status).isEqualTo(-1);
            }

            @Override
            protected void checkCaptures(final String stdOutCapture, final String stdErrCapture) {
                assertThat(stdOutCapture).isEqualTo(messageStdOut + '\n');
                assertThat(stdErrCapture).isEqualTo(messageStdErr + '\n');
            }
        };
        captureJob.run();
    }

    public void should_stop_capturing_if_an_exception_is_raised() {
        try {
            CliCaptureJob captureJob = new CliCaptureJob() {
                @Override
                protected void execute() {
                    throw new RuntimeException("ARGH!");
                }

                @Override
                protected void checkStatus(final int status) {
                    fail("checkStatus must not be called");
                }

                @Override
                protected void checkCaptures(final String stdOutCapture, final String stdErrCapture) {
                    assertThat(stdOutCapture).isEmpty();
                    assertThat(stdErrCapture).isEmpty();
                }
            };
            captureJob.run();

            failBecauseExceptionWasNotThrown(RuntimeException.class);
        } catch (RuntimeException e) {
            assertThat(e).hasMessage("ARGH!");
        }
    }
}
