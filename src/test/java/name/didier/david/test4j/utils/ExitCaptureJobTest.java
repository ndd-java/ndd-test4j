package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import org.testng.annotations.Test;

@Test
public class ExitCaptureJobTest {

    public void should_capture_exit_call() {
        ExitCaptureJob captureJob = new ExitCaptureJob() {
            @Override
            protected void execute() {
                System.exit(-1);
            }

            @Override
            protected void checkStatus(final int status) {
                assertThat(status).isEqualTo(-1);
            }
        };
        captureJob.run();
    }

    public void should_stop_capturing_if_an_exception_is_raised() {
        SecurityManager oldSecurityManager = System.getSecurityManager();

        try {
            ExitCaptureJob captureJob = new ExitCaptureJob() {
                @Override
                protected void execute() {
                    throw new RuntimeException("ARGH!");
                }

                @Override
                protected void checkStatus(final int status) {
                    fail("checkStatus must not be called");
                }
            };
            captureJob.run();

            failBecauseExceptionWasNotThrown(RuntimeException.class);
        } catch (RuntimeException e) {
            assertThat(e).hasMessage("ARGH!");
            assertThat(System.getSecurityManager()).isSameAs(oldSecurityManager);
        }
    }
}
