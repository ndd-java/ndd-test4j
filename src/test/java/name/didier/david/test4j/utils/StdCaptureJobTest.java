package name.didier.david.test4j.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static com.google.common.base.Throwables.throwIfUnchecked;

import org.testng.annotations.Test;

@Test
public class StdCaptureJobTest {

    public void should_capture_without_printing() {
        final String messageStdOut = "message to STDOUT 1";
        final String messageStdErr = "message to STDERR 1";

        CustomCaptureJob captureJob = new CustomCaptureJob(messageStdOut, messageStdErr);
        captureJob.run();
        assertThat(captureJob.getReceivedOnStdOut()).isEqualTo(messageStdOut + '\n');
        assertThat(captureJob.getReceivedOnStdErr()).isEqualTo(messageStdErr + '\n');
    }

    public void should_capture_with_printing() {
        final String messageStdOut = "message to STDOUT 2";
        final String messageStdErr = "message to STDERR 2";

        StdCaptor captor = new StdCaptor();

        try {
            captor.startStdOutCapture();
            captor.startStdErrCapture();

            CustomCaptureJob captureJob = new CustomCaptureJob(messageStdOut, messageStdErr, true);
            captureJob.run();
            assertThat(captureJob.getReceivedOnStdOut()).isEqualTo(messageStdOut + '\n');
            assertThat(captureJob.getReceivedOnStdErr()).isEqualTo(messageStdErr + '\n');
        } catch (Exception e) {
            throwIfUnchecked(e);
            throw new RuntimeException(e);
        } finally {
            assertThat(captor.stopStdOutCapture()).isEqualTo(messageStdOut + '\n');
            assertThat(captor.stopStdErrCapture()).isEqualTo(messageStdErr + '\n');
        }
    }

    public void should_stop_capturing_if_an_exception_is_raised() {
        SecurityManager oldSecurityManager = System.getSecurityManager();
        try {
            new ErrorCaptureJob().run();
            failBecauseExceptionWasNotThrown(RuntimeException.class);
        } catch (RuntimeException e) {
            assertThat(e).hasMessage("ARGH!");
            assertThat(System.getSecurityManager()).isSameAs(oldSecurityManager);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static final class CustomCaptureJob
            extends StdCaptureJob {
        private final String toStdOut;
        private final String toStdErr;

        private String receivedOnStdOut;
        private String receivedOnStdErr;

        protected CustomCaptureJob(final String toStdOut, final String toStdErr) {
            super();
            this.toStdOut = toStdOut;
            this.toStdErr = toStdErr;
        }

        protected CustomCaptureJob(final String toStdOut, final String toStdErr, final boolean print) {
            super(print, print);
            this.toStdOut = toStdOut;
            this.toStdErr = toStdErr;
        }

        public String getReceivedOnStdOut() {
            return receivedOnStdOut;
        }

        public String getReceivedOnStdErr() {
            return receivedOnStdErr;
        }

        @Override
        protected void execute() {
            // CSOFF: RegexpCheck +2
            System.out.println(toStdOut);
            System.err.println(toStdErr);
        }

        @Override
        protected void checkCaptures(final String stdOutCapture, final String stdErrCapture) {
            receivedOnStdOut = stdOutCapture;
            receivedOnStdErr = stdErrCapture;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static final class ErrorCaptureJob
            extends StdCaptureJob {

        protected ErrorCaptureJob() {
            super();
        }

        @Override
        protected void execute() {
            throw new RuntimeException("ARGH!");
        }

        @Override
        protected void checkCaptures(final String stdOutCapture, final String stdErrCapture) {
            assertThat(stdOutCapture).isEmpty();
            assertThat(stdErrCapture).isEmpty();
        }
    }
}
