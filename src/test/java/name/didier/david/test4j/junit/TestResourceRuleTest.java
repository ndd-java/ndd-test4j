package name.didier.david.test4j.junit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import name.didier.david.test4j.unit.AbstractTestResourceTestCase;
import name.didier.david.test4j.unit.TestResource;
import name.didier.david.test4j.unit.TestResourceException;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestResourceRuleTest.describe_find_and_load.class,
        TestResourceRuleTest.describe_toPath.class,
        TestResourceRuleTest.describe_injection_of_reference.class,
        TestResourceRuleTest.describe_injection_of_stream.class,
        TestResourceRuleTest.describe_injection_of_content.class })
public class TestResourceRuleTest {

    public static class describe_find_and_load
            extends AbstractTestResourceRuleTestCase {

        public describe_find_and_load() {
            super("describe_find_and_load");
        }

        @Test
        public void should_find_an_existing_file() {
            File file = testResource.asFile(METHOD_PATTERN);
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_file() {
            try {
                testResource.asFile(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        @Test
        public void should_find_an_existing_url() {
            URL url = testResource.asUrl(METHOD_PATTERN);
            File file = new File(url.getPath());
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_url() {
            try {
                testResource.asUrl(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        @Test
        public void should_load_an_existing_content() {
            String content = testResource.asContent(METHOD_PATTERN);
            assertThat(content).isEqualTo(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_content() {
            try {
                testResource.asContent(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_toPath
            extends AbstractTestResourceRuleTestCase {

        public describe_toPath() {
            super("describe_toPath");
        }

        @Test
        public void should_call_the_naming_strategy() {
            assertThat(testResource.toPath("/{p}/{c}/{m}.txt"))
                    .isEqualTo(testResourcesDirectory + "/should_call_the_naming_strategy.txt");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    // TODO test @TestResource inheritance

    public static class describe_injection_of_reference
            extends AbstractTestResourceRuleTestCase {

        private URL urlWithNoAnnotation;
        private File fileWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private URL url;
        @TestResource(FIELD_PATTERN)
        private File file;

        @TestResource(optional = true)
        private URL nonExistingUrl;
        @TestResource(optional = true)
        private File nonExistingFile;

        public describe_injection_of_reference() {
            super("describe_injection_of_reference");
        }

        @Test
        public void should_inject_an_existing_file_when_annotated()
                throws Exception {
            assertThat(file).isEqualTo(testResourceFile("file"));
            assertThat(file).hasContent(testFileContent("file"));
        }

        @Test
        public void should_not_inject_a_non_existing_file_when_annotated() {
            assertThat(nonExistingFile).isNull();
        }

        @Test
        public void should_not_inject_a_file_when_not_annotated() {
            assertThat(fileWithNoAnnotation).isNull();
        }

        @Test
        public void should_inject_an_existing_url_when_annotated() {
            File actualFile = new File(url.getPath());
            assertThat(actualFile).isEqualTo(testResourceFile("url"));
            assertThat(actualFile).hasContent(testFileContent("url"));
        }

        @Test
        public void should_not_inject_a_non_existing_url_when_annotated() {
            assertThat(nonExistingUrl).isNull();
        }

        @Test
        public void should_not_inject_a_url_when_not_annotated() {
            assertThat(urlWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_injection_of_stream
            extends AbstractTestResourceRuleTestCase {

        private InputStream streamWithNoAnnotation;
        private InputStreamReader streamReaderWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private InputStream stream;
        @TestResource(FIELD_PATTERN)
        private InputStreamReader streamReader;

        @TestResource(optional = true)
        private InputStream nonExistingStream;
        @TestResource(optional = true)
        private InputStreamReader nonExistingStreamReader;

        public describe_injection_of_stream() {
            super("describe_injection_of_stream");
        }

        @Test
        public void should_inject_an_existing_stream_when_annotated() {
            assertThat(stream).hasSameContentAs(testStreamContent("stream"));
        }

        @Test
        public void should_not_inject_a_non_existing_stream_when_annotated() {
            assertThat(nonExistingStream).isNull();
        }

        @Test
        public void should_not_inject_a_stream_when_not_annotated() {
            assertThat(streamWithNoAnnotation).isNull();
        }

        @Test
        public void should_inject_an_existing_stream_reader_when_annotated()
                throws Exception {
            assertThat(IOUtils.readLines(streamReader)).containsExactly(testFileContent("streamReader"));
        }

        @Test
        public void should_not_inject_a_non_existing_stream_reader_when_annotated() {
            assertThat(nonExistingStreamReader).isNull();
        }

        @Test
        public void should_not_inject_a_stream_reader_when_not_annotated() {
            assertThat(streamReaderWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_injection_of_content
            extends AbstractTestResourceRuleTestCase {

        private String contentWithNoAnnotation;
        private List<String> contentListWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private String content;
        @TestResource(FIELD_PATTERN)
        private List<String> contentList;

        @TestResource(optional = true)
        private String nonExistingContent;
        @TestResource(optional = true)
        private List<String> nonExistingContentList;

        public describe_injection_of_content() {
            super("describe_injection_of_content");
        }

        @Test
        public void should_inject_an_existing_file_content_when_annotated() {
            assertThat(content).isEqualTo(testFileContent("content"));
        }

        @Test
        public void should_not_inject_a_non_existing_file_content_when_annotated() {
            assertThat(nonExistingContent).isNull();
        }

        @Test
        public void should_not_inject_a_file_content_when_not_annotated() {
            assertThat(contentWithNoAnnotation).isNull();
        }

        @Test
        public void should_inject_an_existing_file_content_as_list_when_annotated() {
            assertThat(contentList).containsExactly(testFileContent("line1"), testFileContent("line2"));
        }

        @Test
        public void should_not_inject_a_non_existing_file_content_as_list_when_annotated() {
            assertThat(nonExistingContentList).isNull();
        }

        @Test
        public void should_not_inject_a_file_content_as_list_when_not_annotated() {
            assertThat(contentListWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract static class AbstractTestResourceRuleTestCase
            extends AbstractTestResourceTestCase {

        @Rule
        public TestResourceRule testResource = new TestResourceRuleEx();

        protected AbstractTestResourceRuleTestCase(String testClassName) {
            super("/name/didier/david/test4j/junit", "TestResourceRuleTest-" + testClassName);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Test resource clearing. */
    public static class TestResourceRuleEx
            extends TestResourceRule {

        @Override
        protected void after(Statement base, FrameworkMethod method, Object target) {
            super.after(base, method, target);

            try {
                for (Field field : target.getClass().getDeclaredFields()) {
                    if (field.getAnnotation(TestResource.class) != null) {
                        field.setAccessible(true);
                        assertThat(field.get(target)).isNull();
                    }
                }
            } catch (Exception e) {
                fail("Error while checking cleared fields", e);
            }
        }
    }
}
