package name.didier.david.test4j.testng;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.testng.IInvokedMethod;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.unit.AbstractTestResourceTestCase;
import name.didier.david.test4j.unit.TestResource;
import name.didier.david.test4j.unit.TestResourceException;

public class TestNgResourceListenerTest {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_find_and_load
            extends AbstractTestResourceListenerTestCase {

        /** injected */
        private TestNgResources testResources;

        protected describe_find_and_load() {
            super("describe_find_and_load");
        }

        @Test
        public void should_find_an_existing_file() {
            File file = testResources.asFile(METHOD_PATTERN);
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_file() {
            try {
                testResources.asFile(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        @Test
        public void should_find_an_existing_url() {
            URL url = testResources.asUrl(METHOD_PATTERN);
            File file = new File(url.getPath());
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_url() {
            try {
                testResources.asUrl(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        @Test
        public void should_load_an_existing_content() {
            String content = testResources.asContent(METHOD_PATTERN);
            assertThat(content).isEqualTo(thisTestFileContent());
        }

        @Test
        public void should_raise_an_exception_for_a_non_existing_content() {
            try {
                testResources.asContent(METHOD_PATTERN);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class describe_toPath
            extends AbstractTestResourceListenerTestCase {

        /** injected */
        private TestNgResources testResources;

        public describe_toPath() {
            super("describe_toPath");
        }

        @Test
        public void should_call_the_naming_strategy() {
            assertThat(testResources.toPath("/{p}/{c}/{m}.txt"))
                    .isEqualTo(testResourcesDirectory + "/should_call_the_naming_strategy.txt");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    // TODO test @TestResource inheritance

    @Test
    public class describe_injection_of_reference
            extends AbstractTestResourceListenerTestCase {

        private URL urlWithNoAnnotation;
        private File fileWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private URL url;
        @TestResource(FIELD_PATTERN)
        private File file;

        @TestResource(optional = true)
        private URL nonExistingUrl;
        @TestResource(optional = true)
        private File nonExistingFile;

        public describe_injection_of_reference() {
            super("describe_injection_of_reference");
        }

        public void should_inject_an_existing_file_when_annotated() {
            assertThat(file).isEqualTo(testResourceFile("file"));
            assertThat(file).hasContent(testFileContent("file"));
        }

        public void should_not_inject_a_non_existing_file_when_annotated() {
            assertThat(nonExistingFile).isNull();
        }

        public void should_not_inject_a_file_when_not_annotated() {
            assertThat(fileWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_url_when_annotated() {
            File actualFile = new File(url.getPath());
            assertThat(actualFile).isEqualTo(testResourceFile("url"));
            assertThat(actualFile).hasContent(testFileContent("url"));
        }

        public void should_not_inject_a_non_existing_url_when_annotated() {
            assertThat(nonExistingUrl).isNull();
        }

        public void should_not_inject_a_url_when_not_annotated() {
            assertThat(urlWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_injection_of_stream
            extends AbstractTestResourceListenerTestCase {

        private InputStream streamWithNoAnnotation;
        private InputStreamReader streamReaderWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private InputStream stream;
        @TestResource(FIELD_PATTERN)
        private InputStreamReader streamReader;

        @TestResource(optional = true)
        private InputStream nonExistingStream;
        @TestResource(optional = true)
        private InputStreamReader nonExistingStreamReader;

        public describe_injection_of_stream() {
            super("describe_injection_of_stream");
        }

        public void should_inject_an_existing_stream_when_annotated() {
            assertThat(stream).hasSameContentAs(testStreamContent("stream"));
        }

        public void should_not_inject_a_non_existing_stream_when_annotated() {
            assertThat(nonExistingStream).isNull();
        }

        public void should_not_inject_a_stream_when_not_annotated() {
            assertThat(streamWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_stream_reader_when_annotated()
                throws Exception {
            assertThat(IOUtils.readLines(streamReader)).containsExactly(testFileContent("streamReader"));
        }

        public void should_not_inject_a_non_existing_stream_reader_when_annotated() {
            assertThat(nonExistingStreamReader).isNull();
        }

        public void should_not_inject_a_stream_reader_when_not_annotated() {
            assertThat(streamReaderWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_injection_of_content
            extends AbstractTestResourceListenerTestCase {

        private String contentWithNoAnnotation;
        private List<String> contentListWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private String content;
        @TestResource(FIELD_PATTERN)
        private List<String> contentList;

        @TestResource(optional = true)
        private String nonExistingContent;
        @TestResource(optional = true)
        private List<String> nonExistingContentList;

        public describe_injection_of_content() {
            super("describe_injection_of_content");
        }

        public void should_inject_an_existing_file_content_when_annotated() {
            assertThat(content).isEqualTo(testFileContent("content"));
        }

        public void should_not_inject_a_non_existing_file_content_when_annotated() {
            assertThat(nonExistingContent).isNull();
        }

        public void should_not_inject_a_file_content_when_not_annotated() {
            assertThat(contentWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_file_content_as_list_when_annotated() {
            assertThat(contentList).containsExactly(testFileContent("line1"), testFileContent("line2"));
        }

        public void should_not_inject_a_non_existing_file_content_as_list_when_annotated() {
            assertThat(nonExistingContentList).isNull();
        }

        public void should_not_inject_a_file_content_as_list_when_not_annotated() {
            assertThat(contentListWithNoAnnotation).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_unsupported_type
            extends AbstractTestResourceListenerTestCase {

        @TestResource
        private Integer unsupportedType;

        public describe_unsupported_type() {
            super("describe_unsupported_type");
        }

        @Test(expectedExceptions = TestResourceException.class)
        public void needATestMethod() {
            // empty
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgResourceListenerEx.class)
    protected abstract static class AbstractTestResourceListenerTestCase
            extends AbstractTestResourceTestCase {

        protected AbstractTestResourceListenerTestCase(String testClassName) {
            super("/name/didier/david/test4j/testng", "TestNgResourceListenerTest-" + testClassName);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Test resource clearing. */
    public static class TestNgResourceListenerEx
            extends TestNgResourceListener {

        @Override
        public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
            super.afterInvocation(method, testResult);

            if (isListening()) {
                try {
                    for (Field field : testResult.getTestClass().getRealClass().getDeclaredFields()) {
                        if (field.getAnnotation(TestResource.class) != null) {
                            field.setAccessible(true);
                            assertThat(field.get(testResult.getInstance())).as(field.toString()).isNull();
                        }
                    }
                } catch (Exception e) {
                    fail("Error while checking cleared fields", e);
                }
            }
        }
    }
}
