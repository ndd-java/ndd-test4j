package name.didier.david.test4j.testng;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgListenerUtils.isListening;

import org.testng.ITestNGListener;
import org.testng.TestListenerAdapter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.reporters.TestHTMLReporter;
import org.testng.reporters.TextReporter;
import org.testng.reporters.VerboseReporter;

import name.didier.david.test4j.coverage.CoverageUtils;

@Test
public class TestNgListenerUtilsTest {

    public void test_constructor() {
        CoverageUtils.coverConstructor(TestNgListenerUtils.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    @SuppressWarnings("unchecked")
    public class describe_isListening {

        private final Class<? extends ITestNGListener> listener1 = TextReporter.class;
        private final Class<? extends ITestNGListener> listener2 = VerboseReporter.class;
        private final Class<? extends ITestNGListener> listener3 = TestHTMLReporter.class;
        private final Class<? extends ITestNGListener> parentListener = TestListenerAdapter.class;

        public void with_single_listener_and_without_any_superclass() {
            assertThat(isListening(new IsListening1(), listener1)).isFalse();
            assertThat(isListening(new IsListening1(), listener2)).isTrue();
            assertThat(isListening(new IsListening1(), listener1, listener2)).isTrue();
            assertThat(isListening(new IsListening1(), listener2, listener1)).isTrue();

            assertThat(isListening(new IsListening1(), parentListener)).isTrue();
            assertThat(isListening(new IsListening1(), parentListener, listener1)).isTrue();
        }

        public void with_single_listener_with_a_superclass() {
            assertThat(isListening(new IsListening2(), listener1)).isFalse();
            assertThat(isListening(new IsListening2(), listener2)).isTrue();
            assertThat(isListening(new IsListening2(), listener1, listener2)).isTrue();
            assertThat(isListening(new IsListening2(), listener2, listener1)).isTrue();

            assertThat(isListening(new IsListening2(), parentListener)).isTrue();
            assertThat(isListening(new IsListening2(), parentListener, listener1)).isTrue();
        }

        public void with_multiple_listeners_and_without_any_superclass() {
            assertThat(isListening(new IsListening11(), listener1)).isTrue();
            assertThat(isListening(new IsListening11(), listener2)).isTrue();
            assertThat(isListening(new IsListening11(), listener3)).isFalse();
            assertThat(isListening(new IsListening11(), listener1, listener2)).isTrue();
            assertThat(isListening(new IsListening11(), listener2, listener1)).isTrue();
            assertThat(isListening(new IsListening11(), listener2, listener1, listener3)).isTrue();

            assertThat(isListening(new IsListening11(), parentListener)).isTrue();
            assertThat(isListening(new IsListening11(), parentListener, listener1)).isTrue();
        }

        public void with_multiple_listeners_and_with_a_superclass() {
            assertThat(isListening(new IsListening12(), listener1)).isTrue();
            assertThat(isListening(new IsListening12(), listener2)).isTrue();
            assertThat(isListening(new IsListening12(), listener3)).isFalse();
            assertThat(isListening(new IsListening12(), listener1, listener2)).isTrue();
            assertThat(isListening(new IsListening12(), listener2, listener1)).isTrue();
            assertThat(isListening(new IsListening12(), listener2, listener1, listener3)).isTrue();

            assertThat(isListening(new IsListening12(), parentListener)).isTrue();
            assertThat(isListening(new IsListening12(), parentListener, listener1)).isTrue();
        }

        public void with_multiple_listeners_and_with_multiple_superclasses() {
            assertThat(isListening(new IsListening13(), listener1)).isTrue();
            assertThat(isListening(new IsListening13(), listener2)).isTrue();
            assertThat(isListening(new IsListening13(), listener3)).isTrue();
            assertThat(isListening(new IsListening13(), listener1, listener2)).isTrue();
            assertThat(isListening(new IsListening13(), listener2, listener1)).isTrue();
            assertThat(isListening(new IsListening13(), listener2, listener1, listener3)).isTrue();

            assertThat(isListening(new IsListening13(), parentListener)).isTrue();
            assertThat(isListening(new IsListening13(), parentListener, listener1)).isTrue();
        }

        // @Listeners(listener2)
        @Listeners(VerboseReporter.class)
        class IsListening1 {
            // empty
        }

        class IsListening2
                extends IsListening1 {
            // empty
        }

        // @Listeners({ listener1, listener2 })
        @Listeners({ TextReporter.class, VerboseReporter.class })
        class IsListening11 {
            // empty
        }

        class IsListening12
                extends IsListening11 {
            // empty
        }

        // @Listeners(listener3)
        @Listeners(TestHTMLReporter.class)
        class IsListening13
                extends IsListening12 {
            // empty
        }
    }
}
