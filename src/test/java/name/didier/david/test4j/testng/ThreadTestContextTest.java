package name.didier.david.test4j.testng;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.lang.reflect.Method;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(singleThreaded = true)
public class ThreadTestContextTest {

    protected ThreadTestContext threadTestContext;
    private boolean shouldFail;

    @BeforeMethod
    public void initialize_thread_context(final Method method) {
        threadTestContext = new ThreadTestContext(method);
    }

    @AfterMethod
    public void check_thread_context(final Method method) {
        if (shouldFail) {
            try {
                threadTestContext.check();
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                assertThat(e).hasMessageContaining(method.getName());
                assertThat(e.getCause()).isInstanceOf(AssertionError.class);
            }
        } else {
            threadTestContext.check();
        }
    }

    public void should_not_fail_the_test_if_no_exception_is_raised() {
        Runnable successfulRunner = new Runnable() {
            @Override
            public void run() {
                assertThat(true).isTrue();
            }
        };
        shouldFail = false;
        threadTestContext.startAndJoin(successfulRunner);
    }

    public void should_fail_the_test_if_an_exception_is_raised() {
        Runnable failingRunner = new Runnable() {
            @Override
            public void run() {
                assertThat(true).isFalse();
            }
        };
        shouldFail = true;
        threadTestContext.startAndJoin(failingRunner);
    }
}
