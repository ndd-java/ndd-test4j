package name.didier.david.test4j.testng;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * From https://github.com/mockito/mockito/tree/master/subprojects/testng/src/test/java/org/mockitousage/testng
 */
@Test
public class TestNgMockitoListenerTest {

    @Listeners(TestNgMockitoListener.class)
    public class AnnotatedFieldsShouldBeInitializedByMockitoTestNGListenerTest {

        @Mock
        List<?> list;
        // @Spy
        // HashMap<?, ?> map;
        @InjectMocks
        SomeType someType;
        @Captor
        ArgumentCaptor<List<?>> captor;

        @Test
        public void ensure_annotated_fields_are_instantiated()
                throws Exception {
            assertThat(list).isNotNull();
            // assertThat(map).isNotNull();
            assertThat(captor).isNotNull();
            assertThat(someType).isNotNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgMockitoListener.class)
    public class CaptorAnnotatedFieldShouldBeClearedTest {

        @Captor
        ArgumentCaptor<String> captor;
        @Mock
        List<String> list;

        @Test
        public void first_test_method_that_uses_captor()
                throws Exception {
            list.add("a");
            list.add("b");

            verify(list, times(2)).add(captor.capture());
            assertThat(captor.getAllValues()).containsOnly("a", "b");
        }

        @Test
        public void second_test_method_that_uses_captor()
                throws Exception {
            list.add("t");
            list.add("u");

            verify(list, times(2)).add(captor.capture());
            assertThat(captor.getAllValues()).containsOnly("t", "u");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgMockitoListener.class)
    public class ConfigurationMethodTest {

        @Mock
        private Map<String, Integer> map;

        @BeforeMethod
        public void some_behavior() {
            when(map.get("the answer to ...")).thenReturn(42);
        }

        @Test
        public void mocks_should_stay_configured_with_behavior() {
            assertThat(map.get("the answer to ...")).isEqualTo(42);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public class DontResetMocksIfNoListenerTest {

        @Mock
        private Map<String, Integer> map;

        @BeforeMethod
        public void init_mocks() {
            MockitoAnnotations.initMocks(this);
            when(map.get("the answer to ...")).thenReturn(42);
        }

        @Test
        public void mock_behavior_not_resetted_1() {
            assertThat(map.get("the answer to ...")).isEqualTo(42);
        }

        @Test
        public void mock_behavior_not_resetted_2() {
            assertThat(map.get("the answer to ...")).isEqualTo(42);
        }

    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgMockitoListener.class)
    public class EnsureMocksAreInitializedBeforeBeforeClassMethodTest {

        @Mock
        Observer observer;

        @BeforeClass
        private void make_sure_mock_is_initialized() {
            assertThat(observer).isNotNull();
        }

        @Test
        public void dummy_test_see_BeforeClass_code() {
            // empty
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public class InitializeChildTestWhenParentHasListenerTest
            extends AbstractParentTestCase {

        @Mock
        Map<?, ?> childMockField;

        @Test
        public void verify_mocks_are_initialized()
                throws Exception {
            assertThat(childMockField).isNotNull();
            assertThat(parentMockField).isNotNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgMockitoListener.class)
    public class MockFieldsShouldBeResetBetweenTestMethodsTest {

        @Mock
        List<String> list;
        // @Spy
        // HashMap hashMap;
        @InjectMocks
        SomeType someType;

        @Mock
        Observable willBrNulled;

        @Test
        public void behaviour_A_without_infection_from_behaviour_B()
                throws Exception {
            // verify mock is clean
            assertThat(list.get(0)).isNull();
            verify(list, never()).add(anyString());

            // local behavior A
            given(list.get(0)).willReturn("A");
            assertThat(list.get(0)).isEqualTo("A");

            list.add("something else after A");
        }

        @Test
        public void behaviour_B_without_infection_from_behaviour_A()
                throws Exception {
            // verify mock is clean
            assertThat(list.get(0)).isNull();
            verify(list, never()).add(anyString());

            // local behavior A
            given(list.get(0)).willReturn("B");
            assertThat(list.get(0)).isEqualTo("B");

            list.add("something else after B");
        }

        @Test
        public void dont_fail_when_reseting_null_field()
                throws Exception {
            willBrNulled = null;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public class ResetMocksInParentTestClassTooTest
            extends AbstractParentTestCase {

        @Test
        public void interact_with_parent_mock()
                throws Exception {
            parentMockField.get("a");
        }

        @Test
        public void verify_zero_interaction_with_parent_mock()
                throws Exception {
            verifyZeroInteractions(parentMockField);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public class TestWithoutListenerShouldNotInitializeAnnotatedFieldsTest {

        @Mock
        List<?> list;
        @Spy
        Map<?, ?> map;
        @InjectMocks
        SomeType someType;
        @Captor
        ArgumentCaptor<List<?>> captor;

        @Test
        public void test_not_annotated_by_MockitoTestNGListener_should_not_touch_annotated_fields()
                throws Exception {
            assertThat(list).isNull();
            assertThat(map).isNull();
            assertThat(captor).isNull();
            assertThat(someType).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Listeners(TestNgMockitoListener.class)
    public abstract class AbstractParentTestCase {

        @Mock
        Map<?, ?> parentMockField;
    }

    // -----------------------------------------------------------------------------------------------------------------

    public static class SomeType {

        List<?> list;
        Map<?, ?> map;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FAILING TESTS ON PURPOSE:
    // -----------------------------------------------------------------------------------------------------------------

    // @Listeners(MockitoTestNGListener.class)
    // @Test(description = "Always failing, shouldn't be listed in 'mockito-testng.xml'")
    // public class FailingOnPurposeBecauseIncorrectAnnotationUsage {
    //
    // @Mock
    // @Spy
    // Map cant_mock_and_spy_at_the_same_time;
    //
    // @Test
    // public void dummy_test_method()
    // throws Exception {
    // }
    // }
    //
    // @Listeners(MockitoTestNGListener.class)
    // @Test(description = "Always failing, shouldn't be listed in 'mockito-testng.xml'")
    // public class FailingOnPurposeBecauseIncorrectStubbingSyntax {
    //
    // @Test(expectedExceptions = InvalidUseOfMatchersException.class)
    // public void incorrect_stubbing_syntax_in_test()
    // throws Exception {
    // mock(PrintStream.class);
    // anyString();
    // anySet();
    // }
    //
    // }
    //
    // @Listeners(MockitoTestNGListener.class)
    // @Test(description = "Always failing, shouldn't be listed in 'mockito-testng.xml'")
    // public class FailingOnPurposeBecauseWrongStubbingSyntaxInConfigurationMethod {
    //
    // @Mock
    // List list;
    //
    // // should fail
    // @BeforeMethod
    // public void some_wrong_stubs() {
    // anyString();
    // }
    //
    // @Test
    // public void here_to_execute_the_config_method()
    // throws Exception {
    // }
    //
    // }
    //
    // @Test(
    // singleThreaded = true,
    // description = "Test that failing tests report a Mockito exception")
    // public class TestNGShouldFailWhenMockitoListenerFailsTest {
    //
    // private final FailureRecordingListener failureRecorder = new FailureRecordingListener();
    //
    // public void report_failure_on_incorrect_annotation_usage()
    // throws Throwable {
    // TestNG testNG = new_TestNG_with_failure_recorder_for(FailingOnPurposeBecauseIncorrectAnnotationUsage.class);
    //
    // testNG.run();
    //
    // assertTrue(testNG.hasFailure());
    // assertThat(failureRecorder.lastThrowable()).isInstanceOf(MockitoException.class);
    // }
    //
    // @Test
    // public void report_failure_on_incorrect_stubbing_syntax_with_matchers_in_test_methods()
    // throws Exception {
    // TestNG testNG = new_TestNG_with_failure_recorder_for(FailingOnPurposeBecauseIncorrectStubbingSyntax.class);
    //
    // testNG.run();
    //
    // assertTrue(testNG.hasFailure());
    // assertThat(failureRecorder.lastThrowable()).isInstanceOf(InvalidUseOfMatchersException.class);
    // }
    //
    // @Test
    // public void report_failure_on_incorrect_stubbing_syntax_with_matchers_in_configuration_methods()
    // throws Exception {
    // TestNG testNG = new_TestNG_with_failure_recorder_for(
    // FailingOnPurposeBecauseWrongStubbingSyntaxInConfigurationMethod.class);
    //
    // testNG.run();
    //
    // assertTrue(testNG.hasFailure());
    // assertThat(failureRecorder.lastThrowable()).isInstanceOf(InvalidUseOfMatchersException.class);
    // }
    //
    // @AfterMethod
    // public void clear_failure_recorder()
    // throws Exception {
    // failureRecorder.clear();
    // }
    //
    // private TestNG new_TestNG_with_failure_recorder_for(Class<?>... testNGClasses) {
    // TestNG testNG = new TestNG();
    // testNG.setVerbose(0);
    // testNG.setUseDefaultListeners(false);
    // testNG.addListener(failureRecorder);
    //
    // testNG.setTestClasses(testNGClasses);
    // return testNG;
    // }
    // }

}
