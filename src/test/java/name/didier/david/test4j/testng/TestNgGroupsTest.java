package name.didier.david.test4j.testng;

import org.testng.annotations.Test;

import name.didier.david.test4j.coverage.CoverageUtils;

@Test
public class TestNgGroupsTest {

    @Test
    public static class describe_TestNgGroups {

        /** May be extended. */
        public void should_be_covered() {
            CoverageUtils.coverConstructor(TestNgGroups.class);
        }
    }
}
