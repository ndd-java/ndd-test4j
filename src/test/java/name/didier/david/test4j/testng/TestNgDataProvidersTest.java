package name.didier.david.test4j.testng;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgDataProviders.BLANK_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.EMPTY_BUT_NOT_NULL_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.EMPTY_LISTS;
import static name.didier.david.test4j.testng.TestNgDataProviders.EMPTY_SETS;
import static name.didier.david.test4j.testng.TestNgDataProviders.EMPTY_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.HMAC_MD5_BASE16;
import static name.didier.david.test4j.testng.TestNgDataProviders.HMAC_MD5_BASE64;
import static name.didier.david.test4j.testng.TestNgDataProviders.HMAC_SHA256_BASE16;
import static name.didier.david.test4j.testng.TestNgDataProviders.HMAC_SHA256_BASE64;
import static name.didier.david.test4j.testng.TestNgDataProviders.NEGATIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgDataProviders.NON_EXISTING_FILES;
import static name.didier.david.test4j.testng.TestNgDataProviders.NULL_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.POSITIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgDataProviders.STRICTLY_NEGATIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgDataProviders.STRICTLY_POSITIVE_NUMBERS;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.testng.annotations.Test;

import com.google.common.io.BaseEncoding;

import name.didier.david.test4j.coverage.CoverageUtils;

@Test
public class TestNgDataProvidersTest {

    public void constructor_should_be_covered() {
        CoverageUtils.coverConstructor(TestNgDataProviders.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NULL_STRINGS)
    public void test_null_strings_provider(final String string) {
        assertThat(string).isNull();
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_STRINGS)
    public void test_empty_strings_provider(final String string) {
        assertThat(isEmpty(string)).isTrue();
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    public void test_blank_strings_provider(final String string) {
        assertThat(isBlank(string)).isTrue();
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_BUT_NOT_NULL_STRINGS)
    public void test_empty_but_not_null_strings_provider(final String string) {
        assertThat(string).isNotNull();
        assertThat(isBlank(string)).isTrue();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = STRICTLY_NEGATIVE_NUMBERS)
    public void test_strictly_negative_numbers_provider(final Number number) {
        assertThat(number.doubleValue()).isNegative();
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NEGATIVE_NUMBERS)
    public void test_negative_numbers_provider(final Number number) {
        assertThat(number.doubleValue()).isLessThanOrEqualTo(0);
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = STRICTLY_POSITIVE_NUMBERS)
    public void test_strictly_positive_numbers_provider(final Number number) {
        assertThat(number.doubleValue()).isPositive();
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = POSITIVE_NUMBERS)
    public void test_positive_numbers_provider(final Number number) {
        assertThat(number.doubleValue()).isGreaterThanOrEqualTo(0);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_LISTS)
    public void test_empty_lists_provider(final List<String> list) {
        if (list == null) {
            // expected
        } else {
            assertThat(list).isEmpty();
        }
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_SETS)
    public void test_empty_sets_provider(final Set<String> set) {
        if (set == null) {
            // expected
        } else {
            assertThat(set).isEmpty();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NON_EXISTING_FILES)
    public void test_non_existing_files_provider(final File file) {
        assertThat(file).doesNotExist();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = HMAC_MD5_BASE16)
    public void test_hmac_md5_base16_provider(final String message, final String signature)
            throws Exception {
        byte[] digest = digest(message, "HmacMD5");
        String actualSignature = BaseEncoding.base16().encode(digest);
        assertThat(actualSignature).isEqualTo(signature.toUpperCase());
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = HMAC_MD5_BASE64)
    public void test_hmac_md5_base64_provider(final String message, final String signature)
            throws Exception {
        byte[] digest = digest(message, "HmacMD5");
        String actualSignature = BaseEncoding.base64().encode(digest);
        assertThat(actualSignature).isEqualTo(signature);
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = HMAC_SHA256_BASE16)
    public void test_hmac_sha256_base16_provider(final String message, final String signature)
            throws Exception {
        byte[] digest = digest(message, "HmacSHA256");
        String actualSignature = BaseEncoding.base16().encode(digest);
        assertThat(actualSignature).isEqualTo(signature.toUpperCase());
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = HMAC_SHA256_BASE64)
    public void test_hmac_sha256_base64_provider(final String message, final String signature)
            throws Exception {
        byte[] digest = digest(message, "HmacSHA256");
        String actualSignature = BaseEncoding.base64().encode(digest);
        assertThat(actualSignature).isEqualTo(signature);
    }

    private static byte[] digest(final String message, final String algorithm)
            throws Exception {
        byte[] secretKeyBytes = TestNgDataProviders.HMAC_KEY.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec secretKey = new SecretKeySpec(secretKeyBytes, algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(secretKey);
        return mac.doFinal(message.getBytes(StandardCharsets.UTF_8));
    }
}
