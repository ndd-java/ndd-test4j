package name.didier.david.test4j.unit;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class DefaultTestResourceNamingStrategyTest {

    private static final String CN = "DefaultTestResourceNamingStrategyTest";
    private static final String ICN = "DefaultTestResourceNamingStrategyTest-InnerClass";
    private static final String FN = "someField";
    private static final String MN = "someMethod";
    private static final String PN = "name/didier/david/test4j/unit";

    private DefaultTestResourceNamingStrategy naming;

    private Class<?> klass;
    private Field field;
    private Method method;

    @SuppressWarnings("unused")
    private String someField;

    @BeforeMethod
    public void beforeMethod() {
        naming = new DefaultTestResourceNamingStrategy();
        klass = DefaultTestResourceNamingStrategyTest.class;
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_process_a_fixed_pattern() {
        assertThat(naming.resourceName("fixed.xml", klass, field, method))
                .isEqualTo("fixed.xml");
    }

    public void should_process_a_pattern_with_only_a_class_placeholder() {
        assertThat(naming.resourceName("class-{c}.xml", klass, field, method))
                .isEqualTo(format("class-%s.xml", CN));
    }

    public void should_process_a_pattern_with_only_a_field_placeholder() {
        assertThat(naming.resourceName("field-{f}.xml", klass, field(), method))
                .isEqualTo(format("field-%s.xml", FN));
    }

    public void should_process_a_pattern_with_only_a_method_placeholder() {
        assertThat(naming.resourceName("method-{m}.xml", klass, field, method()))
                .isEqualTo(format("method-%s.xml", MN));
    }

    public void should_process_a_pattern_with_only_a_package_placeholder() {
        assertThat(naming.resourceName("package-{p}.xml", klass, field, method))
                .isEqualTo(format("package-%s.xml", PN));
    }

    public void should_process_a_pattern_with_all_placeholders() {
        assertThat(naming.resourceName("all-{c}-{f}-{m}-{p}.xml", klass, field(), method()))
                .isEqualTo(format("all-%s-%s-%s-%s.xml", CN, FN, MN, PN));
    }

    public void should_process_a_pattern_with_all_placeholders_and_an_inner_class() {
        assertThat(naming.resourceName("all-{c}-{f}-{m}-{p}.xml", InnerClass.class, field(), method()))
                .isEqualTo(format("all-%s-%s-%s-%s.xml", ICN, FN, MN, PN));
    }

    public void should_process_an_empty_pattern() {
        assertThat(naming.resourceName("", klass, field(), method()))
                .isEqualTo(format("/%s/%s/%s/%s", PN, CN, MN, FN));
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_process_fixed_patterns() {
        assertThat(naming.resourceNames(new String[] { "fixed1.xml", "fixed2.xml" }, klass, field, method))
                .containsExactly("fixed1.xml", "fixed2.xml");
    }

    public void should_process_patterns_with_only_a_class_placeholder() {
        assertThat(naming.resourceNames(new String[] { "class1-{c}.xml", "class2-{c}.xml" }, klass, field, method))
                .containsExactly(format("class1-%s.xml", CN), format("class2-%s.xml", CN));
    }

    public void should_process_pattern_with_only_a_field_placeholder() {
        assertThat(naming.resourceNames(new String[] { "field1-{f}.xml", "field2-{f}.xml" }, klass, field(), method))
                .containsExactly(format("field1-%s.xml", FN), format("field2-%s.xml", FN));
    }

    public void should_process_pattern_with_only_a_method_placeholder() {
        assertThat(naming.resourceNames(new String[] { "method1-{m}.xml", "method2-{m}.xml" }, klass, field, method()))
                .containsExactly(format("method1-%s.xml", MN), format("method2-%s.xml", MN));
    }

    public void should_process_pattern_with_only_a_package_placeholder() {
        assertThat(naming.resourceNames(new String[] { "package1-{p}.xml", "package2-{p}.xml" }, klass, field, method))
                .containsExactly(format("package1-%s.xml", PN), format("package2-%s.xml", PN));
    }

    public void should_process_patterns_with_all_placeholders() {
        assertThat(naming.resourceNames(
                new String[] { "all1-{c}-{f}-{m}-{p}.xml", "all2-{c}-{f}-{m}-{p}.xml" },
                klass, field(), method()))
                        .containsExactly(
                                format("all1-%s-%s-%s-%s.xml", CN, FN, MN, PN),
                                format("all2-%s-%s-%s-%s.xml", CN, FN, MN, PN));
    }

    public void should_process_patterns_with_all_placeholders_and_an_inner_class() {
        assertThat(naming.resourceNames(
                new String[] { "all1-{c}-{f}-{m}-{p}.xml", "all2-{c}-{f}-{m}-{p}.xml" },
                InnerClass.class, field(), method()))
                        .containsExactly(
                                format("all1-%s-%s-%s-%s.xml", ICN, FN, MN, PN),
                                format("all2-%s-%s-%s-%s.xml", ICN, FN, MN, PN));
    }

    public void should_process_empty_patterns() {
        assertThat(naming.resourceNames(new String[0], klass, field(), method()))
                .containsExactly(format("/%s/%s/%s/%s", PN, CN, MN, FN));
    }

    // -----------------------------------------------------------------------------------------------------------------

    private Field testField(final String fieldName) {
        try {
            return getClass().getDeclaredField(fieldName);
        } catch (SecurityException e) {
            throw e;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    private Method testMethod(final String methodName) {
        try {
            return getClass().getDeclaredMethod(methodName);
        } catch (SecurityException e) {
            throw e;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private Field field() {
        return testField("someField");
    }

    private Method method() {
        return testMethod("someMethod");
    }

    // -----------------------------------------------------------------------------------------------------------------

    @SuppressWarnings("unused")
    private void someMethod() {
        // empty
    }

    private static class InnerClass {
        // empty
    }
}
