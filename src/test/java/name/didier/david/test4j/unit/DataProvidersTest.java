package name.didier.david.test4j.unit;

import org.testng.annotations.Test;

import name.didier.david.test4j.coverage.CoverageUtils;

@Test
public class DataProvidersTest {

    public void constructor_should_be_covered() {
        CoverageUtils.coverConstructor(DataProviders.class);
    }

    // See JUnitDataProvidersTest
    // See TestNgDataProvidersTest
}
