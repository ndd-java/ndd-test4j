package name.didier.david.test4j.unit;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class DefaultTestResourceResolutionStrategyTest {

    private static final String EXISTING_RESOURCE_1 = "resource1";
    private static final String EXISTING_RESOURCE_2 = "resource3";
    private static final String NON_EXISTING_RESOURCE = "resource2";
    private static final String PREFIX = "DefaultTestResourceResolutionStrategyTest_";
    private static final File TEST_RESOURCES_DIR = new File("/name/didier/david/test4j/unit");

    private DefaultTestResourceResolutionStrategy resolver;

    @BeforeMethod
    public void beforeMethod() {
        resolver = new DefaultTestResourceResolutionStrategy();
    }

    public void should_find_an_existing_resource_first() {
        URL url = findResourceAmongst(EXISTING_RESOURCE_1);
        assertThat(url.getPath()).endsWith(testPath(EXISTING_RESOURCE_1));
    }

    public void should_find_an_existing_resource_second() {
        URL url = findResourceAmongst(NON_EXISTING_RESOURCE, EXISTING_RESOURCE_2);
        assertThat(url.getPath()).endsWith(testPath(EXISTING_RESOURCE_2));
    }

    public void should_not_find_a_non_existing_resource() {
        URL url = findResourceAmongst(NON_EXISTING_RESOURCE);
        assertThat(url).isNull();
    }

    public void should_not_find_a_non_existing_resource_related_to_another_class() {
        URL url = findResourceAmongst(List.class, EXISTING_RESOURCE_1);
        assertThat(url).isNull();
    }

    // -----------------------------------------------------------------------------------------------------------------

    private URL findResourceAmongst(final Class<?> clazz, final String... suffixes) {
        List<String> resourceNames = new ArrayList<>();
        for (String suffix : suffixes) {
            resourceNames.add(PREFIX + suffix);
        }
        return resolver.findResource(clazz, resourceNames);
    }

    private URL findResourceAmongst(final String... suffixes) {
        return findResourceAmongst(getClass(), suffixes);
    }

    private String testPath(final String suffix) {
        return new File(TEST_RESOURCES_DIR, PREFIX + suffix).getAbsolutePath();
    }
}
