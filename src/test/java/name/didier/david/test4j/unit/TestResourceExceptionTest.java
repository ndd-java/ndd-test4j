package name.didier.david.test4j.unit;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

@Test
public class TestResourceExceptionTest {

    private static final Throwable CAUSE = new Throwable();
    private static final String MESSAGE = "MESSAGE";
    private static final Object INSTANCE = new Object();
    private static final Method METHOD = getMethod();

    private TestResourceException e;

    public void test_TestResourceException_Object_Method() {
        e = new TestResourceException(INSTANCE, METHOD);
        assertThat(e.getCause()).isNull();
        assertThat(e.getMessage()).isNull();
        assertThat(e.getTestInstance()).isEqualTo(INSTANCE);
        assertThat(e.getTestMethod()).isEqualTo(METHOD);
    }

    public void test_TestResourceException_String_Object_Method() {
        e = new TestResourceException(MESSAGE, INSTANCE, METHOD);
        assertThat(e.getCause()).isNull();
        assertThat(e.getMessage()).isEqualTo(MESSAGE);
        assertThat(e.getTestInstance()).isEqualTo(INSTANCE);
        assertThat(e.getTestMethod()).isEqualTo(METHOD);
    }

    public void test_TestResourceException_Throwable_Object_Method() {
        e = new TestResourceException(CAUSE, INSTANCE, METHOD);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isNull();
        assertThat(e.getTestInstance()).isEqualTo(INSTANCE);
        assertThat(e.getTestMethod()).isEqualTo(METHOD);
    }

    public void test_TestResourceException_String_Throwable_Object_Method() {
        e = new TestResourceException(MESSAGE, CAUSE, INSTANCE, METHOD);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isEqualTo(MESSAGE);
        assertThat(e.getTestInstance()).isEqualTo(INSTANCE);
        assertThat(e.getTestMethod()).isEqualTo(METHOD);
    }

    @SuppressWarnings("unused")
    private void testMethod() {
        // empty
    }

    private static Method getMethod() {
        try {
            return TestResourceExceptionTest.class.getDeclaredMethod("testMethod");
        } catch (Exception e) {
            throw new AssertionError("Cannot find the 'testMethod' method", e);
        }
    }
}
