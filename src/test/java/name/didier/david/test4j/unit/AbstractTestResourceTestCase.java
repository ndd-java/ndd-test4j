package name.didier.david.test4j.unit;

import static java.lang.String.format;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public abstract class AbstractTestResourceTestCase {

    protected static final String METHOD_PATTERN = "/{p}/{c}/{m}";
    protected static final String FIELD_PATTERN = "/{p}/{c}/{f}";

    protected String testClassPath;
    protected String testClassName;
    protected String testResourcesDirectory;
    protected TestResourceHandler handler;

    protected AbstractTestResourceTestCase(String testClassPath, String testClassName) {
        super();
        this.testClassPath = testClassPath;
        this.testClassName = testClassName;
        this.testResourcesDirectory = testClassPath + "/" + testClassName;
    }

    protected Field testField(String fieldName) {
        try {
            return getClass().getDeclaredField(fieldName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected String testFileContent(String fileName) {
        return testClassName + "." + fileName;
    }

    protected Method testMethod(String methodName) {
        try {
            return getClass().getMethod(methodName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected String testPath(String fileName) {
        return new File(testResourcesDirectory, fileName).getAbsolutePath();
    }

    protected URL testResourceUrl(String name) {
        String testResourcePath = format("%s/%s", testResourcesDirectory, name);
        return getClass().getResource(testResourcePath);
    }

    protected File testResourceFile(String name) {
        String testResourcePath = format("%s/%s", testResourcesDirectory, name);
        return new File(getClass().getResource(testResourcePath).getPath()).getAbsoluteFile();
    }

    protected InputStream testStreamContent(String name) {
        return IOUtils.toInputStream(testFileContent(name), Charset.defaultCharset());
    }

    protected String thisTestFileContent() {
        return testFileContent(testMethodName());
    }

    protected Method thisTestMethod() {
        return testMethod(testMethodName());
    }

    protected String thisTestPath() {
        return testPath(testMethodName());
    }

    protected String toPath(URL url) {
        return new File(url.getPath()).getAbsolutePath();
    }

    private String testMethodName() {
        // CSOFF: MagicNumberCheck +1
        return Thread.currentThread().getStackTrace()[3].getMethodName();
    }
}
