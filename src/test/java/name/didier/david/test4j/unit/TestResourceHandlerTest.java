package name.didier.david.test4j.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgMockitoListener;

public class TestResourceHandlerTest {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    @Listeners(TestNgMockitoListener.class)
    public class describe_constructor {

        @Mock
        private TestResourceNamingStrategy naming;
        @Mock
        private TestResourceResolutionStrategy resolver;

        public void constructor_should_use_a_default_naming_strategy() {
            TestResourceHandler handler = new TestResourceHandler();
            assertThat(handler.getNamingStrategy()).isInstanceOf(DefaultTestResourceNamingStrategy.class);
        }

        @SuppressWarnings("unused")
        public void constructor_should_reject_a_null_naming_strategy() {
            try {
                new TestResourceHandler(null, resolver);
                failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
            } catch (IllegalArgumentException e) {
                assertThat(e).hasMessageContaining("namingStrategy");
            }
        }

        public void constructor_should_accept_a_naming_strategy() {
            TestResourceHandler handler = new TestResourceHandler(naming, resolver);
            assertThat(handler.getNamingStrategy()).isSameAs(naming);
        }

        public void constructor_should_use_a_default_resolution_strategy() {
            TestResourceHandler handler = new TestResourceHandler();
            assertThat(handler.getResolutionStrategy()).isInstanceOf(DefaultTestResourceResolutionStrategy.class);
        }

        @SuppressWarnings("unused")
        public void constructor_should_reject_a_null_resolution_strategy() {
            try {
                new TestResourceHandler(naming, null);
                failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
            } catch (IllegalArgumentException e) {
                assertThat(e).hasMessageContaining("resolutionStrategy");
            }
        }

        public void constructor_should_accept_a_resolution_strategy() {
            TestResourceHandler handler = new TestResourceHandler(naming, resolver);
            assertThat(handler.getResolutionStrategy()).isSameAs(resolver);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_find_and_load
            extends AbstractTestResourceHandlerTestCase {

        public describe_find_and_load() {
            super("describe_find_and_load");
        }

        public void should_find_an_existing_file() {
            File file = handler.findFile(METHOD_PATTERN, this, thisTestMethod());
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        public void should_raise_an_exception_for_a_non_existing_file() {
            try {
                handler.findFile(METHOD_PATTERN, this, thisTestMethod());
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        public void should_find_an_existing_url() {
            URL url = handler.findUrl(METHOD_PATTERN, this, thisTestMethod());
            File file = new File(url.getPath());
            assertThat(file.getAbsolutePath()).endsWith(thisTestPath());
            assertThat(file).hasContent(thisTestFileContent());
        }

        public void should_raise_an_exception_for_a_non_existing_url() {
            try {
                handler.findUrl(METHOD_PATTERN, this, thisTestMethod());
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }

        public void should_load_an_existing_content() {
            String content = handler.loadContent(METHOD_PATTERN, this, thisTestMethod());
            assertThat(content).isEqualTo(thisTestFileContent());
        }

        public void should_raise_an_exception_for_a_non_existing_content() {
            try {
                handler.loadContent(METHOD_PATTERN, this, thisTestMethod());
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining(METHOD_PATTERN);
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(thisTestMethod());
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public static class describe_toPath
            extends AbstractTestResourceHandlerTestCase {

        public describe_toPath() {
            super("describe_toPath");
        }

        public void should_delegate_to_the_naming_strategy() {
            String pattern = "my pattern";
            Object testInstance = this;
            Method testMethod = thisTestMethod();

            TestResourceNamingStrategy strategy = mock(TestResourceNamingStrategy.class);
            when(strategy.resourceName(pattern, getClass(), null, testMethod)).thenReturn("my/path");

            handler = new TestResourceHandler(strategy, mock(TestResourceResolutionStrategy.class));
            assertThat(handler.toPath(pattern, testInstance, testMethod)).isEqualTo("my/path");

            verify(strategy).resourceName(pattern, getClass(), null, testMethod);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_injection_of_reference
            extends AbstractTestResourceHandlerTestCase {

        private URL urlWithNoAnnotation;
        private File fileWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private URL url;
        @TestResource(FIELD_PATTERN)
        private File file;

        @TestResource(optional = true)
        private URL nonExistingUrl;
        @TestResource(optional = true)
        private File nonExistingFile;

        public describe_injection_of_reference() {
            super("describe_injection_of_reference");
        }

        public void should_inject_an_existing_file_when_annotated() {
            assertThat(file).isEqualTo(testResourceFile("file"));
            assertThat(file).hasContent(testFileContent("file"));
        }

        public void should_not_inject_a_non_existing_file_when_annotated() {
            assertThat(nonExistingFile).isNull();
        }

        public void should_not_inject_a_file_when_not_annotated() {
            assertThat(fileWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_url_when_annotated() {
            File actualFile = new File(url.getPath());
            assertThat(actualFile).isEqualTo(testResourceFile("url"));
            assertThat(actualFile).hasContent(testFileContent("url"));
        }

        public void should_not_inject_a_non_existing_url_when_annotated() {
            assertThat(nonExistingUrl).isNull();
        }

        public void should_not_inject_a_url_when_not_annotated() {
            assertThat(urlWithNoAnnotation).isNull();
        }

        public void should_clear_a_file_field_when_annotated() {
            assertThat(file).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("file"));
            assertThat(file).isNull();
        }

        public void should_clear_a_url_field_when_annotated() {
            assertThat(url).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("url"));
            assertThat(url).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_injection_of_stream
            extends AbstractTestResourceHandlerTestCase {

        private InputStream streamWithNoAnnotation;
        private InputStreamReader streamReaderWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private InputStream stream;
        @TestResource(FIELD_PATTERN)
        private InputStreamReader streamReader;

        @TestResource(optional = true)
        private InputStream nonExistingStream;
        @TestResource(optional = true)
        private InputStreamReader nonExistingStreamReader;

        public describe_injection_of_stream() {
            super("describe_injection_of_stream");
        }

        public void should_inject_an_existing_stream_when_annotated() {
            assertThat(stream).hasSameContentAs(testStreamContent("stream"));
        }

        public void should_not_inject_a_non_existing_stream_when_annotated() {
            assertThat(nonExistingStream).isNull();
        }

        public void should_not_inject_a_stream_when_not_annotated() {
            assertThat(streamWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_stream_reader_when_annotated()
                throws Exception {
            assertThat(IOUtils.readLines(streamReader)).containsExactly(testFileContent("streamReader"));
        }

        public void should_not_inject_a_non_existing_stream_reader_when_annotated() {
            assertThat(nonExistingStreamReader).isNull();
        }

        public void should_not_inject_a_stream_reader_when_not_annotated() {
            assertThat(streamReaderWithNoAnnotation).isNull();
        }

        public void should_clear_a_InputStream_field_when_annotated() {
            assertThat(stream).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("stream"));
            assertThat(stream).isNull();
        }

        public void should_clear_a_InputStreamReader_field_when_annotated() {
            assertThat(streamReader).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("streamReader"));
            assertThat(streamReader).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_injection_of_content
            extends AbstractTestResourceHandlerTestCase {

        private String contentWithNoAnnotation;
        private List<String> contentListWithNoAnnotation;

        @TestResource(FIELD_PATTERN)
        private String content;
        @TestResource(FIELD_PATTERN)
        private List<String> contentList;

        @TestResource(optional = true)
        private String nonExistingContent;
        @TestResource(optional = true)
        private List<String> nonExistingContentList;

        public describe_injection_of_content() {
            super("describe_injection_of_content");
        }

        public void should_inject_an_existing_file_content_when_annotated() {
            assertThat(content).isEqualTo(testFileContent("content"));
        }

        public void should_not_inject_a_non_existing_file_content_when_annotated() {
            assertThat(nonExistingContent).isNull();
        }

        public void should_not_inject_a_file_content_when_not_annotated() {
            assertThat(contentWithNoAnnotation).isNull();
        }

        public void should_inject_an_existing_file_content_as_list_when_annotated() {
            assertThat(contentList).containsExactly(testFileContent("line1"), testFileContent("line2"));
        }

        public void should_not_inject_a_non_existing_file_content_as_list_when_annotated() {
            assertThat(nonExistingContentList).isNull();
        }

        public void should_not_inject_a_file_content_as_list_when_not_annotated() {
            assertThat(contentListWithNoAnnotation).isNull();
        }

        public void should_clear_a_string_field_when_annotated() {
            assertThat(content).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("content"));
            assertThat(content).isNull();
        }

        public void should_clear_a_list_field_when_annotated() {
            assertThat(contentList).isNotNull();
            handler.clearResource(this, thisTestMethod(), testField("contentList"));
            assertThat(contentList).isNull();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public class describe_unsupported_type {

        @TestResource
        private Integer unsupportedType;

        public void should_not_inject_an_unsupported_type(Method method)
                throws Exception {
            TestResourceHandler handler = new TestResourceHandler();

            try {
                handler.inject(this, method);
                failBecauseExceptionWasNotThrown(TestResourceException.class);
            } catch (TestResourceException e) {
                assertThat(e).hasMessageContaining("unsupportedType");
                assertThat(e.getTestInstance()).isEqualTo(this);
                assertThat(e.getTestMethod()).isEqualTo(method);
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract static class AbstractTestResourceHandlerTestCase
            extends AbstractTestResourceTestCase {

        protected AbstractTestResourceHandlerTestCase(String testClassName) {
            super("/name/didier/david/test4j/unit/", "TestResourceHandlerTest-" + testClassName);
        }

        @BeforeMethod
        protected void create_handler() {
            handler = new TestResourceHandler();
        }

        @BeforeMethod(dependsOnMethods = "create_handler")
        protected void inject_resources(Method method) {
            handler.inject(this, method);
        }
    }
}
